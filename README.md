
# Où télécharger l'exécutable

Allez sur le menu à gauche > Deployments > Releases

# Comment compiler le programme

# 1 - Installer Qt6 et Qt Multimedia

## Sur Windows

- Ouvrez "Qt Maintenance Tool" (accessible depuis le menu démarrer normalement)
- Sélectionnez "Ajouter ou supprimer des composants"
- Faites les mises à jour si nécessaire
- Dans le menu "Sélectionner des composants":
  - Sous "Qt"
    - Sous "Qt 6.5.1"
      - Cochez "MinGW 11.2.0 64-bit"
      - Sous "Additional Libraries"
        - Cochez "Qt Multimedia"
- Installez les composants sélectionnés
- Continuez vers la partie 2

## Sur Linux

- Si vous utilisez Wayland:
  - Installez `qt6-qtwayland`et `qt6-qtwayland-devel`
- Si vous avez installé Qt Creator avec le logiciel sur le site de Qt, suivez les instructions pour Windows.
- Si vous avez installé Qt Creator avec votre gestionnaire des paquets:
  - Installez `qt6-qtbase-devel` et `qt6-qtmultimedia-devel`.
- Continuez vers la partie 2

## Les autres

Débrouillez-vous.

# 2 - Créer un kit

- Ouvrez Qt Creator
- Ouvrez le menu "Preferences" ou "Options", soit dans "Edit", soit dans "Tools"
- Allez dans la partie "Kits"
- Si vous n'avez pas de kit Qt 6.5.1 valide:
  - Allez sur l'onglet "Qt Versions"
  - Si "Qt 6.5.1" n'est pas détecté automatiquement:
	  - Cliquez sur "Add" et créez un nouveau kit avec Qt 6.5.1
	  - Retournez dans l'onglet "Kits"
  - Créez un kit Qt6
  - Vérifiez que le compilateur est le bon (généralement MinGW sur Windows, et GCC sur Linux)
  - Pour "Qt Version", sélectionnez la version que vous avez créé ou une version existante de Qt 6.5.1
  - Validez
# 3 - Configurer le projet
- Ouvrez le projet
- Sélectionnez le kit 6.5.1
- Désélectionnez tous les autres kits
- Cliquez sur "Configure Project"
