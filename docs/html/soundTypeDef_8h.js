var soundTypeDef_8h =
[
    [ "BASE_ENGINE_VOLUME", "soundTypeDef_8h.html#a255794696299e36c8cf7c1c3a1c31607", null ],
    [ "BOOSTER_VOLUME", "soundTypeDef_8h.html#abd8bc3424490148fae453b89ffe86568", null ],
    [ "COLLISION_VOLUME", "soundTypeDef_8h.html#a22c913e72e0b7f2a2b3b6e88ada840ec", null ],
    [ "ENGINE_VOLUME_MULTIPLIER", "soundTypeDef_8h.html#a56392e77f911849c27c91dd3029f1239", null ],
    [ "EXPLOSION_VOLUME", "soundTypeDef_8h.html#ac46f3073afce6d1764a182154a66bc68", null ],
    [ "FALL_VOLUME", "soundTypeDef_8h.html#a98d62e05bebe4a243a38252e68765f4c", null ],
    [ "HEAL_VOLUME", "soundTypeDef_8h.html#acb1773968c07f48a476a70f22f87e6c5", null ],
    [ "MUSIC_VOLUME", "soundTypeDef_8h.html#a6cc4dca7e66bb913a7ff817471211cf2", null ],
    [ "VOICE_VOLUME", "soundTypeDef_8h.html#a0fa4dc8432ae5e9da1cf2a4e3a94531b", null ]
];