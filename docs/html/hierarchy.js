var hierarchy =
[
    [ "GPManager", "classGPManager.html", null ],
    [ "QObject", null, [
      [ "Ship", "classShip.html", null ],
      [ "SoundManager", "classSoundManager.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "RaceManager", "classRaceManager.html", null ],
      [ "Widget", "classWidget.html", null ]
    ] ],
    [ "RaceRenderer", "classRaceRenderer.html", null ],
    [ "RenderLine", "structRenderLine.html", null ],
    [ "ScoreLine", "structScoreLine.html", null ],
    [ "Segment", "structSegment.html", null ],
    [ "ShipDisplay", "structShipDisplay.html", null ],
    [ "VisualRoad", "structVisualRoad.html", null ]
];