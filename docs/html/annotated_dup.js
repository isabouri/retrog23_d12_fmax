var annotated_dup =
[
    [ "GPManager", "classGPManager.html", "classGPManager" ],
    [ "RaceManager", "classRaceManager.html", "classRaceManager" ],
    [ "RaceRenderer", "classRaceRenderer.html", "classRaceRenderer" ],
    [ "RenderLine", "structRenderLine.html", "structRenderLine" ],
    [ "ScoreLine", "structScoreLine.html", "structScoreLine" ],
    [ "Segment", "structSegment.html", "structSegment" ],
    [ "Ship", "classShip.html", "classShip" ],
    [ "ShipDisplay", "structShipDisplay.html", "structShipDisplay" ],
    [ "SoundManager", "classSoundManager.html", "classSoundManager" ],
    [ "VisualRoad", "structVisualRoad.html", "structVisualRoad" ],
    [ "Widget", "classWidget.html", "classWidget" ]
];