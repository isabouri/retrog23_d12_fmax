var searchData=
[
  ['racefinished_0',['raceFinished',['../classRaceManager.html#a4beff4d3745c0d0b3d18d0c931c1ddfa',1,'RaceManager']]],
  ['racemanager_1',['RaceManager',['../classRaceManager.html#a6a00291d2cefffcbb67f524c410bc04d',1,'RaceManager::RaceManager(QString filename, int environment, int nbLaps, vector&lt; int &gt; positionStart={1, 2, 3, 4}, QWidget *parent=nullptr)'],['../classRaceManager.html#ac876ba6b8cecb941bb3abed8f6fd84ab',1,'RaceManager::RaceManager(QString filename, int environment, int nbLaps, int nbEnnemies, bool respawnChoice, QWidget *parent=nullptr)']]],
  ['racerenderer_2',['RaceRenderer',['../classRaceRenderer.html#a2652981874516ef36684e70169afa155',1,'RaceRenderer']]],
  ['reset_3',['reset',['../classGPManager.html#aeb7062ffb54f278c382d144c42f85e87',1,'GPManager']]],
  ['respawn_4',['respawn',['../classShip.html#a5dea6c726f0e61d4b3f7c5cac495def6',1,'Ship']]],
  ['roadcolor_5',['roadColor',['../raceFunctions_8cpp.html#a118b0964da94bd7b0b707b8cbabe4920',1,'roadColor(int environment, float distance, float totalTrackLength, float relativeDistance, bool heal):&#160;raceFunctions.cpp'],['../raceFunctions_8h.html#a87151e016350124a6492c99a299fd990',1,'roadColor(int environment, float distance, float totalTrackDistance, float relativeDistance, bool heal):&#160;raceFunctions.cpp']]]
];
