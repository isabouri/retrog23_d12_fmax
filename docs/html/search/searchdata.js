var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwxy~",
  1: "grsvw",
  2: "u",
  3: "gmrstw",
  4: "bcdefgkmnoprsuw~",
  5: "abchilnrstxy",
  6: "dr",
  7: "dglnpru",
  8: "bcefhmpv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fichiers",
  4: "Fonctions",
  5: "Variables",
  6: "Énumérations",
  7: "Valeurs énumérées",
  8: "Macros"
};

