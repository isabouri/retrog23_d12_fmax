var structSegment =
[
    [ "hasBorders", "structSegment.html#ac5502870b998990af40d7eec0102425c", null ],
    [ "hasHeal", "structSegment.html#a3322f5077cb82a0c849f1685a38c0854", null ],
    [ "itsBooster", "structSegment.html#a442fcc368f19fa9bd4296398d66146e2", null ],
    [ "itsLength", "structSegment.html#ae8f314129d309e48285b035eeb284b1f", null ],
    [ "itsTurnIntensity", "structSegment.html#af91c297ca26bf3bd936876768810eab2", null ],
    [ "itsTwist", "structSegment.html#a86534060e17486f508c49035344ff636", null ]
];