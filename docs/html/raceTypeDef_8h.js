var raceTypeDef_8h =
[
    [ "ShipDisplay", "structShipDisplay.html", "structShipDisplay" ],
    [ "RenderLine", "structRenderLine.html", "structRenderLine" ],
    [ "Segment", "structSegment.html", "structSegment" ],
    [ "RaceType", "raceTypeDef_8h.html#a8748bb136f401a7a8b27559d18a51638", [
      [ "NONE", "raceTypeDef_8h.html#a8748bb136f401a7a8b27559d18a51638ac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "GP", "raceTypeDef_8h.html#a8748bb136f401a7a8b27559d18a51638a2ef788c769f156f7be7388e058b5c48e", null ],
      [ "PRACTICE", "raceTypeDef_8h.html#a8748bb136f401a7a8b27559d18a51638a0e28f5dbb329b9c75239ccbaa5223764", null ]
    ] ]
];