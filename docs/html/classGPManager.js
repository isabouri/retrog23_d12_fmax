var classGPManager =
[
    [ "GPManager", "classGPManager.html#a660dfa46bca597d8e90524c4dbe07c87", null ],
    [ "getLastScore", "classGPManager.html#adbff3b331d3672db9eee149aa67f4eba", null ],
    [ "getTotalScore", "classGPManager.html#a1837b24717fa4f22179194b4e2690873", null ],
    [ "reset", "classGPManager.html#aeb7062ffb54f278c382d144c42f85e87", null ],
    [ "setLastScore", "classGPManager.html#aaed1d59d625fe38d6ec2e6a2b0fbfecd", null ],
    [ "updateScores", "classGPManager.html#abfb71efdb86578c12a16cfa2068a77d6", null ]
];