var structRenderLine =
[
    [ "barrier", "structRenderLine.html#a2d51904dcf87b7fe4e9b92083863bc35", null ],
    [ "barrierPoints", "structRenderLine.html#a17b2b50ed0a898efbbf1a74f8ec5292e", null ],
    [ "baseColor", "structRenderLine.html#ad22fd43b0e10b2bd5f6f04d21a282d18", null ],
    [ "boosterColor", "structRenderLine.html#afacaf644dfb8aab4788db161552c554a", null ],
    [ "boosterPoints", "structRenderLine.html#a4a3517d18f724d95ae0f65ec0f6f971a", null ],
    [ "boosters", "structRenderLine.html#a9e7d730dec84d22c2765945cc2bcd82a", null ],
    [ "heal", "structRenderLine.html#a88390996568ff363291b312751d0967b", null ],
    [ "roadPoints", "structRenderLine.html#a32c18f6e7b3a7567dffbe317e9722f5d", null ],
    [ "shadow", "structRenderLine.html#a80d1e9f0fd71e4e4f193fa1061b79dee", null ],
    [ "ships", "structRenderLine.html#af5f5be5beed22d80a15974a4b96e08ec", null ]
];