/**
 * @file soundmanager.h
 * @brief Defines the SoundManager class
 */

#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <QAudioOutput>
#include <QMediaPlayer>

#include "soundTypeDef.h"

/**
 * @brief Manages the sound effects and background music.
 */
class SoundManager : public QObject
{
    Q_OBJECT

    private:
        QAudioOutput *musicAudioOutput; /**< Audio output for music */
        QMediaPlayer *mainIntroPlayer; /**< Player for the main intro music */
        QMediaPlayer *mainLoopPlayer; /**< Player for the main loop music */
        QMediaPlayer *lastIntroPlayer; /**< Player for the last intro music */
        QMediaPlayer *lastLoopPlayer; /**< Player for the last loop music */
        QMediaPlayer *collisionPlayer; /**< Player for the collision sound effect */
        QAudioOutput *collisionOutput; /**< Audio output for collision sound effect */
        QMediaPlayer *fallSoundPlayer; /**< Player for the falling sound effect */
        QAudioOutput *fallSoundOutput; /**< Audio output for falling sound effect */
        QMediaPlayer *boosterPlayer; /**< Player for the boosters sound effect */
        QAudioOutput *boosterOutput; /**< Audio output for boosters sound effect */
        QMediaPlayer *explosionPlayer; /**< Player for the explosion sound effect */
        QAudioOutput *explosionOutput; /**< Audio output for explosion sound effect */
        QMediaPlayer *voicePlayer; /**< Player for the voice sound */
        QAudioOutput *voiceOutput; /**< Audio output for voice sound */
        QMediaPlayer *healPlayer; /**< Player for the heal sound */
        QAudioOutput *healOutput; /**< Audio output for heal sound */
        QMediaPlayer *enginePlayer; /**< Player for the engine of the ship */
        QAudioOutput *engineOutput; /**< Audio output for engine sound */
        bool only1Loop; /**< True if there is only one loop ( */

    public:
        /**
         * @brief Constructs a SoundManager object.
         * @param newMainIntro: The path to the main intro music file.
         * @param newMainLoop: The path to the main loop music file.
         * @param newLastIntro: The path to the last intro music file.
         * @param newLastLoop: The path to the last loop music file.
         * @param parent: The parent QObject.
         */
        SoundManager(QString newMainIntro, QString newMainLoop, QString newLastIntro, QString newLastLoop, QObject *parent = nullptr);

        /**
         * @brief Constructs a SoundManager object.
         * @param newMainIntro: The path to the main intro music file.
         * @param newMainLoop: The path to the main loop music file.
         * @param newLastIntro: The path to the last intro music file.
         * @param newLastLoop: The path to the last loop music file.
         * @param parent: The parent QObject.
         */
        SoundManager(QString newMainIntro, QString newMainLoop, QObject *parent = nullptr);

        ~SoundManager();

    public slots:

        /**
         * @brief start the back ground music
         */
        void startMusic();

        /**
         * @brief stop the back ground music
         */
        void stopMusic();

        /**
         * @brief Slot that is triggered when the main intro music finishes. Stops the intro and starts playing the main loop
         */
        void changeMainSound();

        /**
         * @brief Slot that is triggered when the last intro music finishes. Stops the intro and starts playing the last loop
         */
        void changeLastSound();

        /**
         * @brief Slot that is triggered when the main loop music finishes. Stops the main loop and starts playing the last intro
         */
        void mainToLast();

        /**
         * @brief Slot that waits for the main loop music to finish, then triggers the transition to the last intro music
         */
        void waitMainLoopEnd();

        /**
         * @brief Slot that plays the collision sound effect
         */
        void playCollisionSound();

        /**
         * @brief Slot that plays the falling sound effect
         */
        void playFallingSound();

        /**
         * @brief Slot that plays the booster sound effect
         */
        void playBoosterSound();

        /**
         * @brief Slot that plays the explosion sound effect
         */
        void playExplosionSound();

        /**
         * @brief Slot that plays the heal sound effect
         */
        void playHealSound();

        /**
         * @brief Slot that stop the heal sound when the ship left the heal
         */
        void stopHealSound();

        /**
         * @brief Slot that start the sound of the motor
         */
        void playEngineSound();

        /**
         * @brief Slot that stop the sound of the motor
         */
        void stopEngineSound();

        /**
         * @brief update the level and the speed of the engine sound depending of the speed
         * @param speed: the speed of the ship
         */
        void updateEngineSound(float speed);

        /**
         * @brief Slot that plays the start voice 3....2.....1.....GO!!!!
         */
        void playStart();

        /**
         * @brief Slot that plays the voice which says FINISHED!!!
         */
        void playFinished();
};

#endif // SOUNDMANAGER_H
