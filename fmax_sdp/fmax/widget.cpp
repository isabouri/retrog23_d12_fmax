#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QDirIterator>
#include <QRegularExpressionValidator>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    // Define the minimum size of the widget
    int minWidth = 1098;
    int minHeight = 563;
    setMinimumSize(minWidth,minHeight);
    setGeometry(100, 100, minWidth, minHeight);

    QIcon widgetIcon(":/resources/resources/menu/icon.png");
    setWindowIcon(widgetIcon);

    //init the sound manager of the menu
    itsSoundManager = new SoundManager("qrc:/resources/music/menu/1.ogg",
                                       "qrc:/resources/music/menu/2.ogg");
    itsSoundManager->startMusic();

    GPCanGoNextRace = false;
    itsTypeOfRace = NONE;
    
    // init control
    for (Direction direction : {UP, DOWN, LEFT, RIGHT})
    {
        RaceManager::editControls(direction, 0, 0);
        RaceManager::editControls(direction, 1, 0);
    }

    // temp
    RaceManager::editControls(UP, 0, Qt::Key_Up);
    RaceManager::editControls(DOWN, 0, Qt::Key_Down);
    RaceManager::editControls(LEFT, 0, Qt::Key_Left);
    RaceManager::editControls(RIGHT, 0, Qt::Key_Right);

    // set the first page with play,setting,quit  buttons
    ui->stackedWidget->setCurrentIndex(0);

    //update the title of the window
    setWindowTitle("F-MAX");

    //set the backgound image and scaled it to the good size
    itsBackgroundName = ":/resources/resources/menu/acceuilTitre.png";
    itsMenuBackground = QPixmap(itsBackgroundName);
    itsMenuBackground = itsMenuBackground.scaled(this->size(),Qt::KeepAspectRatioByExpanding,Qt::FastTransformation); // Qt::KeepAspectRatioByExpanding mode is used to crop the edges of the image which exceed

    // define the regular expression to allow only alphabet letters
    itsKeyValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z\\s]*"), this); // create a new instance of QRegularExpressionValidator using the defined regular expression

    // Apply the validator to the line edit for each input
    ui->lineEditForward->setValidator(itsKeyValidator);
    ui->lineEditBrake->setValidator(itsKeyValidator);
    ui->lineEditRight->setValidator(itsKeyValidator);
    ui->lineEditLeft->setValidator(itsKeyValidator);

    // User can only write 1 caractere
    ui->lineEditForward->setMaxLength(1);
    ui->lineEditBrake->setMaxLength(1);
    ui->lineEditRight->setMaxLength(1);
    ui->lineEditLeft->setMaxLength(1);

    //Set the limit for the number of ennemies input
    itsNbEnnemyValidator = new QRegularExpressionValidator(QRegularExpression("[0-3]*"), this);
    ui->nbVaisseauxPractice->setValidator(itsNbEnnemyValidator);
    ui->nbVaisseauxPractice->setMaxLength(1);
    ui->nbVaisseauxPractice->setText("3");

    //Set the limit for the number of turns input
    itsNbLapsValidator = new QRegularExpressionValidator(QRegularExpression("[1-9]*"), this);
    ui->nbToursPractice->setValidator(itsNbLapsValidator);
    ui->nbToursPractice->setMaxLength(1);

    ui->respawnCheckbox->setCheckState(Qt::Checked);

    QPixmap tutoIcon(":/resources/resources/menu/menuTuto.png");
    QPixmap scaledTutoIcon = tutoIcon.scaled(70,70);
    ui->tutoButton->setIcon(scaledTutoIcon);
    ui->tutoButton->setIconSize(scaledTutoIcon.size());
    itsTutoLabel = nullptr;
}

Widget::~Widget()
{
    delete ui;
    delete itsRaceManager;
    delete itsSoundManager;
    delete itsKeyValidator;
    delete itsNbEnnemyValidator;
    delete itsNbLapsValidator;
}

void Widget::startRace()
{   
    QString path = ":/resources/resources/tracks/"+itsTrackName+".txt";

    if (itsTypeOfRace == GP)
    {
        if(itsLastGPScore.empty())
            itsRaceManager = new RaceManager(path, itsEnvironment, itsNbLaps);
        else
            itsRaceManager = new RaceManager(path, itsEnvironment, itsNbLaps, itsLastGPScore);
    }
    else
    {
        itsRaceManager = new RaceManager(path, itsEnvironment,
                                     ui->nbToursPractice->text().toInt(),
                                     ui->nbVaisseauxPractice->text().toInt(),
                                     ui->respawnCheckbox->checkState() == Qt::Checked);
    }
    
    // Save size / position inside the raceManager widget
    QRect widgetGeometry = geometry();
    itsRaceManager->setGeometry(widgetGeometry); // set the resolution of the race manager window to the same resolution of the menu window

    connect(itsRaceManager, &RaceManager::raceFinished, this, &Widget::handleRaceFinished); // the end of the race will destroy the RaceManager window
    //hide the menu window
    itsSoundManager->stopMusic();
    hide();

    itsRaceManager->activateWindow();
    itsRaceManager->show();

    if(itsTypeOfRace == GP)
        itGPRace++;
}

void Widget::changeSettings()
{
    // Get the text entered in each QLineEdit for each direction
    QString userInputUp = ui->lineEditForward->text();
    QString userInputDown = ui->lineEditBrake->text();
    QString userInputLeft = ui->lineEditLeft->text();
    QString userInputRight = ui->lineEditRight->text();

    // Size of the alphabet
    const int alphabetSize = 26;

    // Mapping array for key codes
    int keyMapping[alphabetSize+1]; // Alphabet + space command

    // Fill the keyMapping array with the corresponding key codes for each letter of the alphabet
    for (int i = 0; i < alphabetSize; i++)
    {
        // Convert 'A' + 1 to int type
        keyMapping[i] = static_cast<int>(Qt::Key('A' + i));
    }

    keyMapping[alphabetSize] = Qt::Key_Space;

    // Check if the userInputUp field is not empty to avoid a crash (an empty QString)
    if (!userInputUp.isEmpty())
    {
        // Get the first letter in uppercase
        QChar firstLetter = userInputUp.at(0).toUpper();
        // Calculate the index of the letter in the alphabet
        int index = firstLetter.toLatin1() - 'A';
        // If the user put a space
        if(firstLetter == '_')
        {
            index = alphabetSize;
            ui->lineEditForward->setText("_");
        }
        // Get the corresponding key code from the keyMapping array
        int keyCode = keyMapping[index];
        // Call the RaceManager::editControls function to modify the controls for the UP direction with the new key code
        RaceManager::editControls(UP, 1, keyCode);

    }

    // Check if the userInputDown field is not empty to avoid a crash (an empty QString)
    if (!userInputDown.isEmpty())
    {
        // Get the first letter in uppercase
        QChar firstLetter = userInputDown.at(0).toUpper();
        // Calculate the index of the letter in the alphabet
        int index = firstLetter.toLatin1() - 'A';
        // If the user put a space
        if(firstLetter == '_')
        {
            index = alphabetSize;
        }
        // Get the corresponding key code from the keyMapping array
        int keyCode = keyMapping[index];
        // Call the RaceManager::editControls function to modify the controls for the DOWN direction with the new key code
        RaceManager::editControls(DOWN,1,keyCode);
    }

    // Check if the userInputLeft field is not empty to avoid a crash (an empty QString)
    if (!userInputLeft.isEmpty())
    {
        // Get the first letter in uppercase
        QChar firstLetter = userInputLeft.at(0).toUpper();
        // Calculate the index of the letter in the alphabet
        int index = firstLetter.toLatin1() - 'A';
        // If the user put a space
        if(firstLetter == '_')
        {
            index = alphabetSize;
        }
        // Get the corresponding key code from the keyMapping array
        int keyCode = keyMapping[index];
        // Call the RaceManager::editControls function to modify the controls for the LEFT direction with the new key code
        RaceManager::editControls(LEFT,1,keyCode);
    }

    // Check if the userInputLeft field is not empty to avoid a crash (an empty QString)
    if (!userInputRight.isEmpty())
    {
        // Get the first letter in uppercase
        QChar firstLetter = userInputRight.at(0).toUpper();
        // Calculate the index of the letter in the alphabet
        int index = firstLetter.toLatin1() - 'A';
        // If the user put a space
        if(firstLetter == '_')
        {
            index = alphabetSize;
        }
        // Get the corresponding key code from the keyMapping array
        int keyCode = keyMapping[index];
        // Call the RaceManager::editControls function to modify the controls for the LEFT direction with the new key code
        RaceManager::editControls(RIGHT,1,keyCode);
    }
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    int currentIndex = ui->stackedWidget->currentIndex();

    if(event->key()==Qt::Key_Escape)
    {
        if(currentIndex == 1)
        {
            ui->stackedWidget->setCurrentIndex(0);
            itsBackgroundName = ":/resources/resources/menu/acceuilTitre.png";
            itsMenuBackground = QPixmap(itsBackgroundName);
            update();
            itsMenuBackground = itsMenuBackground.scaled(this->size(),Qt::KeepAspectRatioByExpanding,Qt::FastTransformation);
            changeSettings();
        }
        else if(currentIndex == 2)
            ui->stackedWidget->setCurrentIndex(8);
        else if(currentIndex == 3)
            ui->stackedWidget->setCurrentIndex(6);
        else if(currentIndex == 4)
            ui->stackedWidget->setCurrentIndex(8);
        else if(currentIndex == 5)
            ui->stackedWidget->setCurrentIndex(8);
        else if(currentIndex == 6)
            ui->stackedWidget->setCurrentIndex(8);
        else if(currentIndex == 7 and not GPCanGoNextRace)
        {
            ui->stackedWidget->setCurrentIndex(0);
            itsTypeOfRace = NONE;
        }
        else if(currentIndex == 8)
            ui->stackedWidget->setCurrentIndex(0);
    }

    if (itsTypeOfRace == GP and GPCanGoNextRace)
        if(event->key()==Qt::Key_Space or
           event->key()==Qt::Key_Escape or
           event->key()==Qt::Key_Enter or
           event->key()==Qt::Key_Return)
            startNextGPRace();
}

void Widget::handleRaceFinished()
{
    // Save size/position
    QRect widgetGeometry = itsRaceManager->geometry();
    setGeometry(widgetGeometry); // set the resolution of the menu window to the same resolution of the race manager window

    // Get the score of the last race
    if (itsTypeOfRace == GP)
    {
        itsLastGPScore = itsRaceManager->getScoreEndGame();

        for (int i=0; i<4; ++i)
            itsGPManager.setLastScore(i, itsLastGPScore[i]);
    }

    //close the race manager window
    itsRaceManager->close();

    itsSoundManager->startMusic();
    
    delete itsRaceManager;

    if (itsTypeOfRace == GP)
    {
        vector<ScoreLine> ships(4);
        for (int i=0; i<4; ++i)
        {
            ships[i].totalScore = itsGPManager.getTotalScore(i);
            ships[i].lastScore = itsGPManager.getLastScore(i);
            ships[i].shipNumber = i;
            switch (i)
            {
                case 0:
                    ships[i].name = "Blue Falcon"; break;
                case 1:
                    ships[i].name = "Fire Stingray"; break;
                case 2:
                    ships[i].name = "Golden Fox"; break;
                case 3:
                    ships[i].name = "Wild Goose"; break;
            }
        }

        sort(ships.begin(), ships.end(), [](const ScoreLine& ship1, const ScoreLine& ship2)
        {
            return ship1.totalScore+ship1.lastScore > ship2.totalScore+ship2.lastScore;
        });

        int index = 0;
        for (ScoreLine ship: ships)
        {
            switch (index)
            {
                case 0:
                    ui->lScore10->setText(ship.name);
                    ui->lScore11->setText(QString::number(ship.lastScore));
                    ui->lScore12->setText(QString::number(ship.totalScore+ship.lastScore));
                    if (ship.name == "Blue Falcon")
                        ui->lScore10->setStyleSheet("QLabel{color: blue;}");
                    else
                        ui->lScore10->setStyleSheet("QLabel{color: white;}");
                    break;
                case 1:
                    ui->lScore20->setText(ship.name);
                    ui->lScore21->setText(QString::number(ship.lastScore));
                    ui->lScore22->setText(QString::number(ship.totalScore+ship.lastScore));
                    if (ship.name == "Blue Falcon")
                        ui->lScore20->setStyleSheet("QLabel{color: blue;}");
                    else
                        ui->lScore20->setStyleSheet("QLabel{color: white;}");
                    break;
                case 2:
                    ui->lScore30->setText(ship.name);
                    ui->lScore31->setText(QString::number(ship.lastScore));
                    ui->lScore32->setText(QString::number(ship.totalScore+ship.lastScore));
                    if (ship.name == "Blue Falcon")
                        ui->lScore30->setStyleSheet("QLabel{color: blue;}");
                    else
                        ui->lScore30->setStyleSheet("QLabel{color: white;}");
                    break;
                case 3:
                    ui->lScore40->setText(ship.name);
                    ui->lScore41->setText(QString::number(ship.lastScore));
                    ui->lScore42->setText(QString::number(ship.totalScore+ship.lastScore));
                    if (ship.name == "Blue Falcon")
                        ui->lScore40->setStyleSheet("QLabel{color: blue;}");
                    else
                        ui->lScore40->setStyleSheet("QLabel{color: white;}");
                    break;
            }
            ++index;
        }

        itsGPManager.updateScores();

        ui->stackedWidget->setCurrentIndex(7);
        show();

        GPCanGoNextRace = (itGPRace != itsGPListRace.end());
    }
    else
    {
        if (itsTypeOfRace == GP and itGPRace == itsGPListRace.end())
        {
            ui->stackedWidget->setCurrentIndex(7);
            GPCanGoNextRace = true;
        }
        else
            ui->stackedWidget->setCurrentIndex(0);
        show();

        itsGPListRace.clear();
        itsLastGPScore.clear();
        itsGPManager.reset();
    }
}

void Widget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter * painter = new QPainter(this); // the painter used to display the background image

    short int gap = 0; // store the amount of pixels needed to move the background image on the left

    if(itsMenuBackground.size().width() > this->size().width())                // if the width of the background is lower than the width of the window
    {
        gap = -(itsMenuBackground.size().width() - this->size().width())/2;    // the image is moved to half of the exceeding pixels on the left
    }

    painter->drawPixmap(gap,0,itsMenuBackground); // display the background image
    delete painter;
}

void Widget::resizeEvent(QResizeEvent *event)
{
    //scale the image to the new resolution
    itsMenuBackground = QPixmap(itsBackgroundName); // if the scale of the window changed, we scale the base background image
    itsMenuBackground = itsMenuBackground.scaled(event->size(),Qt::KeepAspectRatioByExpanding,Qt::FastTransformation);
    update(); // call Widget::paintEvent(QPaintEvent *event)
}

void Widget::on_quitButton_clicked()
{
    close(); // close the window if the quit button is clicked
}

void Widget::on_playButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(8); // the window which display all gameModes
}

void Widget::on_returnButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0); // the menu window
    itsBackgroundName = ":/resources/resources/menu/acceuilTitre.png";
    itsMenuBackground = QPixmap(itsBackgroundName);
    update();
    itsMenuBackground = itsMenuBackground.scaled(this->size(),Qt::KeepAspectRatioByExpanding,Qt::FastTransformation);
}

void Widget::on_settingsButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1); // the settings menu
    itsBackgroundName = ":/resources/resources/menu/settingsBackground.png";
    itsMenuBackground = QPixmap(itsBackgroundName);
    update();
    itsMenuBackground = itsMenuBackground.scaled(this->size(),Qt::KeepAspectRatioByExpanding,Qt::FastTransformation);
}

void Widget::on_returnButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(0); // the menu window
    itsBackgroundName = ":/resources/resources/menu/acceuilTitre.png";
    itsMenuBackground = QPixmap(itsBackgroundName);
    update();
    itsMenuBackground = itsMenuBackground.scaled(this->size(),Qt::KeepAspectRatioByExpanding,Qt::FastTransformation);

    changeSettings();
}

void Widget::startNextGPRace()
{
    GPCanGoNextRace = false;
    if (itsTypeOfRace == GP and itGPRace == itsGPListRace.end())
        ui->stackedWidget->setCurrentIndex(0);
    else
    {
        itsTrackName = itGPRace->first;
        itsNbLaps = itGPRace->second.first;
        itsEnvironment = itGPRace->second.second;
        startRace();
    }

}

void Widget::on_grandPrixButton_clicked()
{
    itsTypeOfRace = GP;
    ui->stackedWidget->setCurrentIndex(6); // the menu to select the cup
}

void Widget::on_practiceButton_clicked()
{
    itsTypeOfRace = PRACTICE;
    ui->stackedWidget->setCurrentIndex(6); // the menu to select the cup to select a map
}

void Widget::on_pushButton_Iron_Practice_clicked()
{
    if (itsTypeOfRace == PRACTICE)
    {
        ui->stackedWidget->setCurrentIndex(2); //The menu to select a track from iron cup
    }
    else
    {
        itsGPListRace.push_back(make_pair("bean",make_pair(5,1)));
        itsGPListRace.push_back(make_pair("d2",make_pair(3,2)));
        itsGPListRace.push_back(make_pair("swing",make_pair(3,3)));
        itsGPListRace.push_back(make_pair("8",make_pair(5,4)));
        itsGPListRace.push_back(make_pair("maxime",make_pair(3,3)));

        itGPRace = itsGPListRace.begin();
        itsTrackName = itGPRace->first;
        itsNbLaps = itGPRace->second.first;
        itsEnvironment = itGPRace->second.second;
        startRace();
    }
}

void Widget::on_pushButton_Bean_clicked()
{
    ui->stackedWidget->setCurrentIndex(3); //The menu to select a track from ruby cup
    itsTrackName = "bean";
    itsEnvironment = 1;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_D2_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "d2";
    itsEnvironment = 2;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_Swing_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "swing";
    itsEnvironment = 3;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_8__clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "8";
    itsEnvironment = 4;
    ui->nbToursPractice->setText("5");
}

void Widget::on_pushButton_Maxime_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "maxime";
    itsEnvironment = 3;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_Titanium_Practice_clicked()
{
    if (itsTypeOfRace == PRACTICE)
        ui->stackedWidget->setCurrentIndex(4); //The menu to select a track from titanium cup
    else if(itsTypeOfRace == GP)
    {
        itsGPListRace.push_back(make_pair("y",make_pair(3,2)));
        itsGPListRace.push_back(make_pair("tron",make_pair(3,4)));
        itsGPListRace.push_back(make_pair("t-rex",make_pair(3,3)));
        itsGPListRace.push_back(make_pair("81",make_pair(3,1)));
        itsGPListRace.push_back(make_pair("zen",make_pair(3,4)));

        itGPRace = itsGPListRace.begin();
        itsTrackName = itGPRace->first;
        itsNbLaps = itGPRace->second.first;
        itsEnvironment = itGPRace->second.second;
        startRace();
    }
}

void Widget::on_pushButton_Y_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "y";
    itsEnvironment = 2;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_Tron_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "tron";
    itsEnvironment = 4;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_TRex_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "t-rex";
    itsEnvironment = 3;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_81__clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "81";
    itsEnvironment = 1;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_Zen_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "zen";
    itsEnvironment = 4;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_Ruby_Practice_clicked()
{
    if (itsTypeOfRace == PRACTICE)
        ui->stackedWidget->setCurrentIndex(5); //The menu to select a track from ruby cup
    else if(itsTypeOfRace == GP)
    {
        itsGPListRace.push_back(make_pair("lucky_you",make_pair(3,2)));
        itsGPListRace.push_back(make_pair("vega_missyl",make_pair(5,1)));
        itsGPListRace.push_back(make_pair("pride_road",make_pair(3,5)));
        itsGPListRace.push_back(make_pair("space_traveller",make_pair(3,3)));
        itsGPListRace.push_back(make_pair("spider_den",make_pair(1,6)));

        itGPRace = itsGPListRace.begin();
        itsTrackName = itGPRace->first;
        itsNbLaps = itGPRace->second.first;
        itsEnvironment = itGPRace->second.second;
        startRace();
    }
}

void Widget::on_pushButton_Lucky_You_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "lucky_you";
    itsEnvironment = 2;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_VegaMissyl_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "vega_missyl";
    itsEnvironment = 1;
    ui->nbToursPractice->setText("5");
}

void Widget::on_pushButton_PridesRoad_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "pride_road";
    itsEnvironment = 5;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_SpaceTraveller_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "space_traveller";
    itsEnvironment = 3;
    ui->nbToursPractice->setText("3");
}

void Widget::on_pushButton_SpiderDen_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    itsTrackName = "spider_den";
    itsEnvironment = 6;
    ui->nbToursPractice->setText("1");
}

void Widget::on_returnToSelectCup_Ruby_clicked()
{
    ui->stackedWidget->setCurrentIndex(6); // the menu to select the cup to select a map
}

void Widget::on_returnToSelectCup_Titanium_clicked()
{
    ui->stackedWidget->setCurrentIndex(6);
}

void Widget::on_returnToSelectCup_Iron_clicked()
{
    ui->stackedWidget->setCurrentIndex(6);
}

void Widget::on_returnToMenuMode_clicked()
{
    ui->stackedWidget->setCurrentIndex(8);
}

void Widget::on_returnToChoseMap_clicked()
{
    ui->stackedWidget->setCurrentIndex(6);
}

void Widget::on_nbToursPractice_editingFinished()
{
    QString turnsPractice = ui->nbToursPractice->text();
    if(turnsPractice.isEmpty())
        ui->nbToursPractice->setText("3");
}

void Widget::on_startRaceAfterParam_clicked()
{
    startRace();
}

void Widget::on_lineEditForward_editingFinished()
{
    if(ui->lineEditForward->text() == " ")
        ui->lineEditForward->setText("_");
}

void Widget::on_lineEditBrake_editingFinished()
{
    if(ui->lineEditBrake->text() == " ")
        ui->lineEditBrake->setText("_");
}

void Widget::on_lineEditLeft_editingFinished()
{
    if(ui->lineEditLeft->text() == " ")
        ui->lineEditLeft->setText("_");
}

void Widget::on_lineEditRight_editingFinished()
{
    if(ui->lineEditRight->text() == " ")
        ui->lineEditRight->setText("_");
}

void Widget::on_tutoButton_clicked()
{
    //display the tutorial pop-up window
    popupDialog.setWindowTitle("Tutorial");

    QPixmap tutoImage(":/resources/resources/menu/imageTuto.png");
    if (itsTutoLabel != nullptr)
        delete itsTutoLabel;
    itsTutoLabel = new QLabel(&popupDialog);
    itsTutoLabel->setScaledContents(true);
    itsTutoLabel->setFixedSize(1000,550);
    itsTutoLabel->setPixmap(tutoImage);
    QVBoxLayout layout(&popupDialog);
    popupDialog.setFixedSize(1000,550);


    if(popupDialog.isVisible())
        popupDialog.hide();
    else
        popupDialog.show();

}

void Widget::on_nbVaisseauxPractice_editingFinished()
{
    QString shipsPractice = ui->nbVaisseauxPractice->text();
    if(shipsPractice.isEmpty())
        ui->nbVaisseauxPractice->setText("1");
}


void Widget::on_stackedWidget_currentChanged(int arg1)
{
    if (arg1 == 0)
        itsTypeOfRace = NONE;
}

