/**
 * @file shipTypeDef.h
 * @brief Defines an enum and a constant
 */

#ifndef SHIPTYPEDEF_H
#define SHIPTYPEDEF_H

#include <utility>
using namespace std;

#define PI 3.1415926535f

/**
 * @brief Enum used for input directions
 */
enum Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

/**
 * @brief Struct used to define the caracteristics of the road at a given point
 */
struct VisualRoad
{
    float averageTurn; /**< Turn amount */
    float averageTwist; /**< Twist amount */
    bool barriers[261]; /**< Presence of barriers for the next 261 distance units */
    bool heals[261]; /**< Presence of healing zones for the next 261 distance units */
    pair<bool, float> boosters[261]; /**< Presence and location of boosters for the next 261 distance units */
};

#endif // SHIPTYPEDEF_H
