#include "racemanager.h"
#include "ui_racemanager.h"

map<Direction, int[2]> RaceManager::controls; // the map containing the controls set by the players

// GP constructor
RaceManager::RaceManager(QString fileName, int environment, int nbLaps, vector<int> positionStart, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RaceManager),
    itsEnvironment(environment)
{
    itsRaceType = GP;
    ui->setupUi(this);
    QIcon widgetIcon(":/resources/resources/menu/icon.png");
    setWindowIcon(widgetIcon);

    //update the title of the window
    setWindowTitle("F-MAX");

    // define the minimal size of the window
    int minWidth = 1098;
    int minHeight = 563;
    setMinimumSize(minWidth,minHeight);

    //hide the mouse during the game
    this->setCursor(Qt::BlankCursor);

    //load the track
    loadTrack(fileName);

    //init the number of turns for the race from value read on track select
    itsNbLaps=nbLaps;

    //init the renderer
    itsRenderer = new RaceRenderer(itsTotalTrackLength, environment);

    //set the size of the screen
    itsRenderer->setScreenSize(this->size());

    //display a countdown
    startCounter = new QTimer(this);
    startCounter->start(1000);
    QObject::connect(startCounter, SIGNAL(timeout()), this, SLOT(startGame()));

    //display a countdown
    raceCounter = nullptr;

    //timer use to d
    stopTrack = new QTimer(this);

    // create the timer use to move the ship and count the duration of the raod
    timer = new QTimer(this);

    //create the ships
    vector<int>::iterator positionShip = positionStart.begin();
    itsPlayer = new Ship("blue_falcon", *positionShip-1);
    itsShips = new vector<Ship *>;
    itsShips->push_back(itsPlayer);
    advance(positionShip, 1);
    itsShips->push_back(new Ship("fire_stingray", *positionShip-1));
    advance(positionShip, 1);
    itsShips->push_back(new Ship("golden_fox", *positionShip-1));
    advance(positionShip, 1);
    itsShips->push_back(new Ship("wild_goose", *positionShip-1));

    //init the sound manager with music of the current level
    if (itsEnvironment == 6)
    {
        itsSoundManager = new SoundManager("qrc:/resources/music/env_6/1.ogg",
                                           "qrc:/resources/music/env_6/2.ogg");
    }
    else
    {
        itsSoundManager = new SoundManager("qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/1.ogg",
                                           "qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/2.ogg",
                                           "qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/3.ogg",
                                           "qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/4.ogg");
    }

    itsSoundManager->startMusic();
    itsSoundManager->playStart();
    itsSoundManager->playEngineSound();
    //connect the colision sound
    connect(itsPlayer, SIGNAL(collision()), this->itsSoundManager, SLOT(playCollisionSound()));

    //connect the falling sound
    connect(itsPlayer, SIGNAL(fall()), this->itsSoundManager, SLOT(playFallingSound()));

    //connect the explosion sound
    connect(itsPlayer, SIGNAL(explosion()), this->itsSoundManager, SLOT(playExplosionSound()));

    //connect the booster sound
    connect(itsPlayer, SIGNAL(boosterSound()), this->itsSoundManager, SLOT(playBoosterSound()));

    //connect the heal sound
    connect(itsPlayer, SIGNAL(onHeal()), this->itsSoundManager, SLOT(playHealSound()));

    //connect to stop the heal sound
    connect(itsPlayer, SIGNAL(notOnHeal()), this->itsSoundManager, SLOT(stopHealSound()));

    for (Ship *ship : *itsShips)
        ship->setIsRespawnable(true);

    //add the currnet player to the renderer
    itsRenderer->setPlayer(itsPlayer);

    //init the lifebar
    itsLifeBar = new QRect(10, 60, 200, 25);

    //init the borderLifebar
    borderLifeBar = new QRect(8, 58, 203, 28);
    changePlayerRacePosition();
    
    //display the position in race before the game started
    ui->positionInRace->setText(QString::number(itsPlayer->getRacePostion()) + "/" + QString::number(itsShips->size()));

    itsGameOver = false;
    itsLastLap = false;
}

// Practice constructor
RaceManager::RaceManager(QString fileName, int environment, int nbLaps, int nbEnnemies, bool respawnChoice, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RaceManager),
    itsEnvironment(environment)
{
    itsRaceType = PRACTICE;
    ui->setupUi(this);
    QIcon widgetIcon(":/resources/resources/menu/icon.png");
    setWindowIcon(widgetIcon);

    //update the title of the window
    setWindowTitle("F-MAX");

    // define the minimal size of the window
    int minWidth = 1098;
    int minHeight = 563;
    setMinimumSize(minWidth,minHeight);

    //hide the mouse during the game
    this->setCursor(Qt::BlankCursor);

    //load the track
    loadTrack(fileName);

    //init the number of turns for the race from value read on track select
    itsNbLaps=nbLaps;

    //init the number of AI ennemies
    itsNbEnnemies=nbEnnemies;

    //init the renderer
    itsRenderer = new RaceRenderer(itsTotalTrackLength, environment);

    //set the size of the screen
    itsRenderer->setScreenSize(this->size());

    //display a countdown
    startCounter = new QTimer(this);
    startCounter->start(1000);
    QObject::connect(startCounter, SIGNAL(timeout()), this, SLOT(startGame()));

    //display a countdown
    raceCounter = nullptr;

    //timer use to d
    stopTrack = new QTimer(this);

    // create the timer use to move the ship and count the duration of the raod
    timer = new QTimer(this);

    //create the player ship
    itsPlayer = new Ship("blue_falcon", 1);

    //init the sound manager with music of the current level
    if (itsEnvironment == 6)
    {
        itsSoundManager = new SoundManager("qrc:/resources/music/env_6/1.ogg",
                                           "qrc:/resources/music/env_6/2.ogg");
    }
    else
    {
        itsSoundManager = new SoundManager("qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/1.ogg",
                                           "qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/2.ogg",
                                           "qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/3.ogg",
                                           "qrc:/resources/music/env_"+QString::number(itsEnvironment)+"/4.ogg");
    }

    itsSoundManager->startMusic();
    itsSoundManager->playStart();
    itsSoundManager->playEngineSound();
    //connect the colision sound
    connect(itsPlayer, SIGNAL(collision()), this->itsSoundManager, SLOT(playCollisionSound()));

    //connect the falling sound
    connect(itsPlayer, SIGNAL(fall()), this->itsSoundManager, SLOT(playFallingSound()));

    //connect the explosion sound
    connect(itsPlayer, SIGNAL(explosion()), this->itsSoundManager, SLOT(playExplosionSound()));

    //connect the booster sound
    connect(itsPlayer, SIGNAL(boosterSound()), this->itsSoundManager, SLOT(playBoosterSound()));

    //connect the heal sound
    connect(itsPlayer, SIGNAL(onHeal()), this->itsSoundManager, SLOT(playHealSound()));

    //connect to stop the heal sound
    connect(itsPlayer, SIGNAL(notOnHeal()), this->itsSoundManager, SLOT(stopHealSound()));

    //create opponents ships
    itsShips = new vector<Ship *>;
    itsShips->push_back(itsPlayer);
    switch(itsNbEnnemies){
        case 1:
            itsShips->push_back(new Ship("fire_stingray", 0));
            break;
        case 2:
            itsShips->push_back(new Ship("fire_stingray", 0));
            itsShips->push_back(new Ship("golden_fox", 2));
            break;
        case 3:
            itsShips->push_back(new Ship("fire_stingray", 0));
            itsShips->push_back(new Ship("golden_fox", 2));
            itsShips->push_back(new Ship("wild_goose", 3));
            break;
        default:
            break;
    }

    for (Ship *ship : *itsShips)
        ship->setIsRespawnable(respawnChoice);

    //add the currnet player to the renderer
    itsRenderer->setPlayer(itsPlayer);

    //init the lifebar
    itsLifeBar = new QRect(10, 60, 200, 25);

    //init the borderLifebar
    borderLifeBar = new QRect(8, 58, 203, 28);
    changePlayerRacePosition();
    
    //display the position in race before the game started
    ui->positionInRace->setText(QString::number(itsPlayer->getRacePostion()) + "/" + QString::number(itsShips->size()));

    itsGameOver = false;

    itsLastLap = false;
}

RaceManager::~RaceManager()
{
    delete ui;
    delete timer;
    delete stopTrack;
    delete itsLifeBar;
    delete borderLifeBar;
    delete itsRenderer;
    delete startCounter;
    delete raceCounter;
    delete itsSoundManager;
    // itsPlayer is deleted in this loop
    for (Ship *ship : *itsShips)
        delete ship;
    delete itsShips;
}

void RaceManager::editControls(Direction direction, int number, int key)
{
    //update controls
    controls[direction][number] = key;
}

void RaceManager::loadTrack(QString filename)
{
    //open the file
    QFile trackFile(filename);
    if (!trackFile.open(QIODevice::ReadOnly | QIODevice::Text))
        throw "Impossible d'ouvrir le fichier de circuit: "+filename.toStdString();

    itsTotalTrackLength = 0;

    //start the reading of the track
    QTextStream in(&trackFile);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QStringList values = line.split(' ');
        Segment segment;
        segment.itsLength = values[0].toInt();
        segment.itsTurnIntensity = values[1].toFloat();
        segment.itsTwist = - values[2].toFloat();
        segment.hasBorders = values[3].toInt();
        segment.hasHeal = values[4].toInt();
        if (values[5].toInt() != -1)
            segment.itsBooster = {true, values[5].toFloat()};
        else
            segment.itsBooster = {false, 0};
        itsTotalTrackLength += segment.itsLength;
        itsSegments.push_back(segment);
    }
}

VisualRoad RaceManager::determineRoadAtDist(float targetDistance)
{
    VisualRoad res;
    res.averageTurn = 0;
    res.averageTwist = 0;

    // compute average turn and twist over next 261 distance units
    int segmentIndex = 0;
    // first, find the begin segment
    int totalDistance = 0;
    while ((totalDistance + itsSegments[segmentIndex].itsLength) < targetDistance)
    {
        totalDistance += itsSegments[segmentIndex].itsLength;
        ++segmentIndex;
        if (segmentIndex == (int)itsSegments.size())
            segmentIndex = 0;
    }

    int distanceInSegment = targetDistance - totalDistance;

    int currentRelativeDistance = 0;
    while (currentRelativeDistance < 261)
    {
        // compute and store all informations about the part of the which will be displayed
        res.averageTurn += itsSegments[segmentIndex].itsTurnIntensity;
        res.averageTwist+= itsSegments[segmentIndex].itsTwist;
        res.barriers[currentRelativeDistance] = itsSegments[segmentIndex].hasBorders;
        res.heals[currentRelativeDistance] = itsSegments[segmentIndex].hasHeal;
        res.boosters[currentRelativeDistance] = itsSegments[segmentIndex].itsBooster;

        ++distanceInSegment;
        if (distanceInSegment > itsSegments[segmentIndex].itsLength)
        {
            distanceInSegment = 0;
            ++segmentIndex;
            if (segmentIndex == (int)itsSegments.size())
                segmentIndex = 0;
        }

        ++currentRelativeDistance;
    }

    res.averageTurn /= 261;
    res.averageTwist /= 261;

    return res;
}

void RaceManager::gameEnd(bool win)
{
    //play the FINISHED!!!! sound and stop all sound exept the music
    itsSoundManager->playFinished();
    itsSoundManager->stopHealSound();
    itsSoundManager->stopEngineSound();

    itsGameOver = true;
    // display the message for win/lose
    if (itsRaceType == PRACTICE)
    {
        if (win)
        {
            ui->cdLabel->setStyleSheet("QLabel { color : #00FF22; }");
            ui->cdLabel->setText("YOU WIN");
        }
        else
        {
            ui->cdLabel->setStyleSheet("QLabel { color : red; }");
            ui->cdLabel->setText("YOU LOSE");

        }
    }

    //hide useless race information
    ui->loopCpt->hide();
    ui->speedLCD->hide();
    ui->label->hide();
    ui->lap->hide();

    for (Ship *ship : *itsShips)
        ship->setScore(ship->getScore()+(itsShips->size()+1-ship->getRacePostion()));

    // Let the ship slow down
    stopTrack->start(6000);
    QObject::connect(stopTrack, SIGNAL(timeout()), this, SLOT(stopTimer()));
}

void RaceManager::setNbEnnemies(int nbEnnemies)
{
    itsNbEnnemies = nbEnnemies;
}

void RaceManager::gameLoop()
{
    //change the volume and the speed of the motorsound depending of the speed
    itsSoundManager->updateEngineSound(itsPlayer->getForwardSpeed());

    //update the lap cpt
    ui->loopCpt->setText(QString::number(((int)itsPlayer->getDistance()/itsTotalTrackLength)+1)+"/"+QString::number(itsNbLaps));

    //change the music if the player is on the final lap
    if(!itsLastLap and ((int)itsPlayer->getDistance()/itsTotalTrackLength) == itsNbLaps - 1 )
    {
        itsSoundManager->waitMainLoopEnd();
        itsLastLap = true;
    }

    // update position of all ships
    for (Ship *ship : *itsShips)
    {
        VisualRoad atShip = determineRoadAtDist(ship->getTrueDistance());
        if (ship == itsPlayer)
            ship->move(itsUserInput, atShip.barriers[0], atShip.heals[0], atShip.boosters[0], atShip.averageTurn, itsGameOver);
        else//if the ship is AI controlled, we use the moveAI method instead
            ship->moveAI(atShip, itsGameOver);
    }
    ui->speedLCD->display(QString::number((int)(itsPlayer->getForwardSpeed()*208)));

    // check for collision between ships
    for (Ship *ship : *itsShips)
        for (Ship *other : *itsShips)
            if (ship != other and ship->getFallState()==0 and other->getFallState()==0)
            {
                float distanceDistance = fmod(other->getTrueDistance(), itsTotalTrackLength) - fmod(ship->getTrueDistance(), itsTotalTrackLength);
                float distancePosition = (ship->getPosition() - other->getPosition())*30;
                if (abs(distanceDistance) < 3 and abs(distancePosition) < 3)
                {
                    //play the sound of the collision
                    if (ship==itsPlayer)
                        itsSoundManager->playCollisionSound();

                    ship->bump(atan2(distancePosition, distanceDistance), 1 + 2.3*sqrt(abs(ship->getForwardSpeed() - other->getForwardSpeed())+0.05));
                    ship->updatePower(-0.04);
                }
            }
    itsRenderer->updateCameraPosition();
    update();

    //the following line "itsPlayer->getDistance()/itsTotalLengthTrack" return the number of lap done
    if (not itsGameOver)
    {
        changePlayerRacePosition();
        ui->positionInRace->setText(QString::number(itsPlayer->getRacePostion()) + "/" + QString::number(itsShips->size()));

        float maxEnnemyDistance = 0;
        for (Ship *ship: *itsShips)
            if (ship!=itsPlayer and ship->getTrueDistance() > maxEnnemyDistance)
                maxEnnemyDistance = ship->getTrueDistance();

        if(itsPlayer->getDistance()/itsTotalTrackLength == itsNbLaps or (itsPlayer->getItsPower() == 0 and not itsPlayer->getIsRespawnable()))
        {
            if (itsPlayer->getIsRespawnable())
                gameEnd(itsPlayer->getTrueDistance() > maxEnnemyDistance);
            else
                gameEnd(itsPlayer->getTrueDistance() > maxEnnemyDistance and itsPlayer->getItsPower() > 0);
        }
    }
}

void RaceManager::startGame()
{
    //get the first value of the counter (3 by default in the racemanager.ui)
    short int value = ui->cdLabel->text().toInt();
    if ((itsPlayer->getItsPower() == 0) and not itsPlayer->getIsRespawnable())
    {
        startCounter->stop();
        gameEnd(false);
    }
    else if(value > 1)
    {
        //decrease the counter each second
        ui->cdLabel->setText(QString::number(value-1));
    }
    else if (value == 1)
    {
        //when the count down is finished, dispplay GO !!!
        ui->cdLabel->setText("GO !!!");
        // start the timer, 16ms gap means 60 frame per second
        timer->start(16);
        QObject::connect(timer, SIGNAL(timeout()), this, SLOT(gameLoop()));
        // start the count of the game
        raceCounter = new time_point<steady_clock>;
        *raceCounter = steady_clock::now();
    }
    else
    {
        //empty the label
        ui->cdLabel->setText("");
        startCounter->stop();
    }

}

void RaceManager::stopTimer()
{
    //stop all remainings timer
    timer->stop();
    stopTrack->stop();

    if (itsRaceType == GP)
        emit raceFinished();
}

void RaceManager::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter * painter = new QPainter(this);

    VisualRoad here = determineRoadAtDist(itsPlayer->getDistance());

    //call the raceRenderer attribute to compute and display the game
    itsRenderer->drawManager(   painter,
                                here.averageTurn,
                                here.averageTwist,
                                here.barriers,
                                here.heals,
                                here.boosters,
                                itsShips);

    //init the color of lifeBar
    QColor c(0,0,0);
    c.setHsv(itsPlayer->getItsPower()*220,255,255);


    // Draw the life bar
    painter->setBrush(c);
    painter->drawRect(*itsLifeBar);


    if (itsPlayer->getItsPower() > 0)
    {
        itsLifeBar->setWidth(itsPlayer->getItsPower()*200);
    }
    else
    {
        itsLifeBar->setWidth(0);
    }

    // Draw the border life bar
    painter->setPen(Qt::white);
    painter->setBrush(Qt::transparent);
    painter->drawRect(*borderLifeBar);

    painter->end();

    if (not itsGameOver)
    {
        //update the time counter
        QString digitClock = "";

        if (raceCounter != nullptr)
        {
            time_point<steady_clock> end = steady_clock::now();
            auto duration = duration_cast<milliseconds>(end - *raceCounter);


            // minutes
            digitClock += ((duration.count()/60000)%99 > 9)?QString::number((duration.count()/60000)%99):"0"+QString::number((duration.count()/60000)%99);

            // second
            digitClock += ":";
            digitClock += ((duration.count()/1000)%60 > 9)?QString::number((duration.count()/1000)%60):"0"+QString::number((duration.count()/1000)%60);


            // hundredth
            digitClock += ".";
            digitClock += ((duration.count()/10)%99 > 9)?QString::number((duration.count()/10)%99):"0"+QString::number((duration.count()/10)%99);
        }
        else
        {
            digitClock = "00:00.00";
        }

        ui->raceCount->setText(digitClock);
    }

    //update the lap counter depending on the amount of turns for the map
    ui->loopCpt->setText(QString::number(((int)itsPlayer->getDistance()/itsTotalTrackLength)+1)+"/"+QString::number(itsNbLaps));

    delete painter;
}

void RaceManager::resizeEvent(QResizeEvent *event)
{
    //resize the image displayed by the raceRenderer attribute
    itsRenderer->setScreenSize(event->size());
}

void RaceManager::keyPressEvent(QKeyEvent *event)
{
    int k = event->key();
    for (Direction direction : {UP, DOWN, LEFT, RIGHT})
        if (k == controls[direction][0] or k == controls[direction][1])
        {
            //update ship's direction info with pressed keys
            itsUserInput[direction] = true;
            return;
        }
    if (itsRaceType==PRACTICE)
        if (k == Qt::Key_Escape or k==Qt::Key_Enter) // stop the game if the escape key is pressed
            emit raceFinished();

}

void RaceManager::keyReleaseEvent(QKeyEvent *event)
{
    int k = event->key();
    for (Direction direction : {UP, DOWN, LEFT, RIGHT})
        if (k == controls[direction][0] or k == controls[direction][1])
        {
            //resets the information of the direction given by the player to 0
            itsUserInput[direction] = false ;
            return;
        }
}

void RaceManager::changePlayerRacePosition()
{
    vector<Ship *> *copyListShips = new vector<Ship *>(*itsShips);
    sort(copyListShips->begin(), copyListShips->end(),
         [](Ship *ship1, Ship *ship2) {
             return ship1->getTrueDistance() < ship2->getTrueDistance();
         });

    int id = copyListShips->size();
    for (Ship *ship : *copyListShips) {
        ship->setRacePostion(id);
        id--;

        // Change QLabel color depending of the position of the player
        switch (itsPlayer->getRacePostion())
        {
            case 1:
                // Blue for the first place
                ui->positionInRace->setStyleSheet("color:cyan;");
                break;
            case 2:
                // Green for the second place
                ui->positionInRace->setStyleSheet("color:lime;");
                break;
            case 3:
                // purple for the third place
                ui->positionInRace->setStyleSheet("color:violet;");
                break;
            default:
                // White fot the white place
                ui->positionInRace->setStyleSheet("color:red;");
        }
    }

    delete copyListShips;
}

vector<int> RaceManager::getScoreEndGame()
{
    vector<int> listOfPoints;
    //Parcours tous les vaisseau affin de recuperer leurs score et de l'ajouter dans la liste a renvoyer
    for (Ship *ship : *itsShips)
        listOfPoints.push_back(ship->getScore());
    return listOfPoints;
}
