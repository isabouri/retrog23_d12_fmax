/**
 * @file gpmanager.h
 * @brief Defines the GPManager class
 */

#ifndef GPMANAGER_H
#define GPMANAGER_H

#include <vector>
using namespace std;

/**
 * @brief The GPManager class handles keep track of the score during a grand prix game
 */
class GPManager
{
    private:
        vector<int> itsTotalScores; /**< Current score of each racer */
        vector<int> itsLastScores; /**< Score obtained by each racer in the last race */
    public:
        /**
         * @brief GPManager constructor
         */
        GPManager();

        /**
         * @brief Adds the score of the lastest race to the total score, and reset the last score
         */
        void updateScores();

        /**
         * @brief Resets all scores
         */
        void reset();

        int getTotalScore(int index) const;
        int getLastScore(int index) const;
        void setLastScore(int index, int lastScore);
};

#endif // GPMANAGER_H
