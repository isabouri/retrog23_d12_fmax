#include "ship.h"

Ship::Ship(QString name, int startPosition, QObject *parent) : QObject(parent)
{
    itsTrueDistance = -(10 + 20*startPosition);
    itsPosition = ((startPosition+1)%3)*0.25 + 0.25;
    itsDirection = 0;
    itsMaxForwardSpeed = 5;
    itsPossibleForwardSpeed = itsMaxForwardSpeed;
    itsForwardSpeed = 0;

    itsMaxSideSpeed = 0.015;
    itsPossibleSideSpeed = itsMaxSideSpeed;
    itsSideSpeed = 0;
    itsPower = 1;
    itsFallState = 0;
    itsScore = 0;
    isOut = false;
    isHealSignalEmited = false;

    itsSprites.resize(24);
    for (int i=0; i<24; ++i)
    {
        QString imagePath = QString(":/resources/resources/sprites/"+name+"/%1.png").arg(QString::number(i).rightJustified(3, '0'));
        itsSprites[i] = new QImage(imagePath);
    }

    itsSpritesExplosion.resize(4);
    for (int i=0; i<4; ++i)
    {
        QString imagePath = QString(":/resources/resources/sprites/explosion/explosion_%1.png").arg(QString::number(i+1));
        itsSpritesExplosion[i] = new QImage(imagePath);
    }
    itsRespawnTimer = nullptr;
}

Ship::~Ship()
{
    if (itsRespawnTimer != nullptr)
        delete itsRespawnTimer;
    for (QImage *sprite: itsSprites)
        delete sprite;
    for (QImage *sprite: itsSpritesExplosion)
        delete sprite;
}

QImage *Ship::getSprite(float angle)
{
    if (itsPower == 0 and itsFallState == 0)
    {
        if (itsRespawnTimer != nullptr)
        {
            if(!isOut)
            {
                emit explosion();
                isOut = true;
            }
            if (1800<itsRespawnTimer->remainingTime() and itsRespawnTimer->remainingTime()<3000)
            {
                int phase = (3000-itsRespawnTimer->remainingTime())/60;
                if (phase%4 < 3)
                    return itsSpritesExplosion[phase%4];
                else if (phase > 11)
                    return itsSpritesExplosion[3];
            }
            else
                return itsSpritesExplosion[3];
        }
        angle = 0.5;
    }
    else
    {
        angle /= 2*3.14159265; // Convert radians to fraction 0 to 1
        angle += 0.01*itsFallState;
    }

    // normalize
    angle = fmod(angle, 1.0);
    if (angle < 0)
        angle += 1;

    int spriteNumber;
    if (angle < 9/32.) // Less than 90 degrees counter-clockwise
    {
        angle *= 32;
        angle += 0.5;
        spriteNumber = angle;
    }
    else if (angle < 3/4. + 1/64.) // Facing the camera
    {
        angle *= 16;
        angle += 4;
        angle += 0.5;
        spriteNumber = angle;
    }
    else if (angle < 63/64.) // More than 270 degrees (less than 90 clockwise) but not back to 0
    {
        angle *= 32;
        angle -= 8;
        angle += 0.5;
        spriteNumber = angle;
    }
    else // almost full circle
    {
        spriteNumber = 0;
    }

    return itsSprites[spriteNumber];
}

void Ship::moveAI(VisualRoad atShip, bool gameOver)
{
    // Check if out of the track (no brders)
    if(((itsPosition < -0.05 or itsPosition > 1.05) and (not atShip.barriers[0])) or itsFallState != 0)
    {
        --itsFallState;
    }

    map<Direction, bool> AIInput;
    AIInput[UP] = true;
    AIInput[LEFT] = false;
    AIInput[RIGHT] = false;
    AIInput[DOWN] = false;

    bool isThereBooster = false;
    float firstBooster;

    // check if there is a booster on sight
    if (abs(atShip.averageTurn)<0.4)
    {
        for (int i=0; i<261; i += 5) // no need to check every line
            if (atShip.boosters[i].first)
            {
                isThereBooster = true;
                firstBooster = atShip.boosters[i].second;
                i = 261; // safe break
            }
    }

    // determine input
    if (isThereBooster) // Go to next booster if visible
    {
        // If not on booster
        if (itsPosition<(firstBooster-0.12))
            AIInput[RIGHT] = true;
        else if ((firstBooster+0.12)<itsPosition)
            AIInput[LEFT] = true;
    }
    else if (atShip.averageTurn == 0) // Go back to the center
    {
        if (itsPosition < 0.4)
            AIInput[RIGHT] = true;
        else if (itsPosition > 0.6)
            AIInput[LEFT] = true;
    }
    else if (abs(atShip.averageTurn) < 0.3)
    {
        if ((itsPosition < 0.25 or itsPosition > 0.75) and itsForwardSpeed > 0.1*itsPossibleForwardSpeed)
            AIInput[UP] = false;

        if (itsPosition < 0.33)
            AIInput[RIGHT] = true;
        else if (itsPosition > 0.66)
            AIInput[LEFT] = true;
    }
    else
    {
        float targetMaxSpeed = tanh(-3*(abs(1.1*atShip.averageTurn)-0.7)) + 2;
        float brakeMargin = 0.9*(abs(atShip.averageTurn)-0.2);
        float slowDownMargin = abs(atShip.averageTurn)-0.1;
        float noTurnMargin = 0.7;
        if (atShip.averageTurn > 0)
        {
            AIInput[RIGHT] = true;
            if (itsPosition < brakeMargin)
            {
                if (itsForwardSpeed > targetMaxSpeed and itsPosition < 0.5)
                    AIInput[DOWN] = true;
            }
            else if (itsPosition < slowDownMargin)
            {
                if (itsForwardSpeed > targetMaxSpeed-0.2 and itsPosition < 0.5)
                    AIInput[UP] = false;
            }
            else if (itsPosition > noTurnMargin)
                AIInput[RIGHT] = false;
        }
        if (atShip.averageTurn < 0)
        {
            AIInput[LEFT] = true;
            if (itsPosition > 1-brakeMargin)
            {
                if (itsForwardSpeed > targetMaxSpeed and itsPosition > 0.5)
                    AIInput[DOWN] = true;
            }
            else if (itsPosition > 1-slowDownMargin)
            {
                if (itsForwardSpeed > targetMaxSpeed-0.2 and itsPosition > 0.5)
                    AIInput[UP] = false;
            }
            else if (itsPosition < 1-noTurnMargin)
                AIInput[LEFT] = false;
        }
    }

    move(AIInput, atShip.barriers[0], atShip.heals[0], atShip.boosters[0], atShip.averageTurn, gameOver, false);
}

void Ship::move(map<Direction, bool> userInput, bool border, bool heal, pair<bool, double> booster, float turn, bool gameOver, bool playSound)
{
    // Check if out of the track (no brders)
    if(((itsPosition < -0.05 or itsPosition > 1.05) and (not border)) or itsFallState != 0)
    {
        --itsFallState;
        if(isHealSignalEmited)
            emit notOnHeal();
        //emit the signal to play the sound of falling
        if(!isOut and playSound)
        {
            emit fall();
            isOut = true;
        }

    }

    if (itsFallState == 0 and !gameOver and itsPower!=0)
    {
        // Update speeds
        if (userInput[UP] and !userInput[DOWN])
        {
            // Accelerating
            if (booster.first and (booster.second-0.125)<itsPosition and itsPosition<(booster.second+0.125))
            {
                itsForwardSpeed = itsForwardSpeed*0.9 + 10*0.1;
                emit boosterSound();
            }
            else
                itsForwardSpeed = itsForwardSpeed*0.99 + itsPossibleForwardSpeed*0.01;
        }
        else if (!userInput[UP] and !userInput[DOWN])
        {
            // Releasing
            if (booster.first and (booster.second-0.125)<itsPosition and itsPosition<(booster.second+0.125))
            {
                itsForwardSpeed = itsForwardSpeed*0.95 + 6*0.05;
                emit boosterSound();
            }
            else
                itsForwardSpeed = itsForwardSpeed/1.01;
            if (itsForwardSpeed < 0.2)
                itsForwardSpeed = 0;
        }
        else
        {
            // Braking
            itsForwardSpeed -= 0.05;
            if (itsForwardSpeed < 0.2)
                itsForwardSpeed = 0;
        }

        // Going faster reduces turn
        int directionInput = userInput[RIGHT]-userInput[LEFT];
        float x = itsForwardSpeed/itsMaxForwardSpeed;
        float y = tanh(3*x)*(1-0.04*x) + 0.05;
        if (y<0.01)
            y=0;
        itsPossibleSideSpeed = y*itsMaxSideSpeed;
        itsSideSpeed = itsSideSpeed*0.5 + (itsPossibleSideSpeed*directionInput)*0.5;

        // Turning reduces speed
        x = abs(itsSideSpeed)/itsMaxSideSpeed;
        //y = pow(1 - pow(x*2/3 - 0.05, 4), 3);
        y = 1 - pow(x*2/3 - 0.05, 4);
        itsPossibleForwardSpeed = y * itsMaxForwardSpeed;

        // Check if border collides
        if((itsPosition < 0.05 or itsPosition > 0.95) and border)
        {
            if (playSound)
                emit collision();
            
            if(userInput[UP] or itsForwardSpeed > itsMaxForwardSpeed*0.1)
                itsForwardSpeed = itsForwardSpeed*0.9 + (itsMaxForwardSpeed/10)*0.1; // Reduce the speed
            else
                itsForwardSpeed = itsForwardSpeed*0.9;
            updatePower(-0.012);

            if (itsPosition < 0.05)
                bump(PI/2, 1.5*exp(-itsPosition));
            else
                bump(-PI/2, 1.5*exp(itsPosition-1));
        }

        //heal the ship if it is on heal
        if (heal)
        {
            if(!isHealSignalEmited)
            {
                emit onHeal();
                isHealSignalEmited = true;

            }
            updatePower(0.01);
        }
        else if(isHealSignalEmited)
        {
            emit notOnHeal();
            isHealSignalEmited = false;
        }

    }
    else if(gameOver)
    {
        itsForwardSpeed = itsForwardSpeed/1.01;
        if (itsForwardSpeed < 0.2)
            itsForwardSpeed = 0;
        itsSideSpeed=0;
    }
    else if (itsPower == 0)
    {
        itsForwardSpeed = itsForwardSpeed/1.1;
        if (itsForwardSpeed < 0.2)
            itsForwardSpeed = 0;
        itsSideSpeed=0;
    }
    else
    {
        itsForwardSpeed /= 1.02;
        itsSideSpeed /= 1.02;
        updatePower(-0.02);
    }

    // Apply movement
    itsTrueDistance += itsForwardSpeed;
    itsPosition += itsSideSpeed;
    itsDirection = -30*itsSideSpeed;

    //Push the car out of the track when there is a turn, except if the race is finished
    if (not (gameOver or itsPower == 0))
        itsPosition -= itsForwardSpeed*tanh(turn)/120;

    // check if the ship is out of power
    if (itsPower==0 and itsRespawnTimer == nullptr)
    {
        // create the timer use to delay the respawn after the death
        itsRespawnTimer = new QTimer();

        // respawn in 3 seconds
        itsRespawnTimer->start(3000);
        if (isRespawnable)
            connect(itsRespawnTimer, SIGNAL(timeout()), this, SLOT(respawn()));
    }
}

void Ship::bump(float angle, float strength)
{
    //compute the strenght of the bump
    float forwardSpeedImpact = strength * -cos(angle)/10;
    float sideSpeedImpact = strength * sin(angle)/100;

    itsSideSpeed += cap(-1.1f*itsMaxSideSpeed, sideSpeedImpact, 1.1f*itsMaxSideSpeed);
    itsForwardSpeed += cap(-1.1f*itsMaxForwardSpeed, forwardSpeedImpact, 1.1f*itsMaxForwardSpeed);
}

void Ship::setIsRespawnable(bool newIsRespawnable)
{
    isRespawnable = newIsRespawnable;
}

bool Ship::getIsRespawnable() const
{
    return isRespawnable;
}

void Ship::respawn()
{
    itsRespawnTimer->stop();
    delete itsRespawnTimer;
    itsRespawnTimer = nullptr;
    itsForwardSpeed = 0;
    itsPosition = 0.5;
    itsFallState = 0;
    itsPower = 0.5;
    isOut = false;
}

int Ship::getDistance() const
{
    return (int)itsTrueDistance;
}

float Ship::getPosition() const
{
    return itsPosition;
}

float Ship::getDirection() const
{
    return itsDirection;
}

float Ship::getForwardSpeed() const
{
    return itsForwardSpeed;
}

float Ship::getItsPower() const
{
    return itsPower;
}

int Ship::getFallState() const
{
    return itsFallState;
}

double Ship::getTrueDistance() const
{
    return itsTrueDistance;
}

int Ship::getRacePostion() const
{
    return itsRacePostion;
}

void Ship::setRacePostion(int racePostion)
{
    itsRacePostion = racePostion;
}

int Ship::getScore() const
{
    return itsScore;
}

void Ship::setScore(int newScore)
{
    itsScore = newScore;
}

void Ship::updatePower(float power)
{
    itsPower += power;
    if (itsPower > 1)
    {
        itsPower = 1;
    }
    else if (itsPower < 0)
    {
        itsPower = 0;
    }
}
