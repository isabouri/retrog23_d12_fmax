QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    gpmanager.cpp \
    main.cpp \
    raceFunctions.cpp \
    raceRenderer.cpp \
    racemanager.cpp \
    ship.cpp \
    soundmanager.cpp \
    widget.cpp

HEADERS += \
    gpmanager.h \
    raceFunctions.h \
    raceRenderer.h \
    raceTypeDef.h \
    racemanager.h \
    ship.h \
    shipTypeDef.h \
    soundTypeDef.h \
    soundmanager.h \
    typeDef.h \
    widget.h

FORMS += \
    racemanager.ui \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    music_env_1.qrc \
    music_env_2.qrc \
    music_env_3.qrc \
    music_env_4.qrc \
    music_env_5.qrc \
    music_env_6.qrc \
    music_menu.qrc \
    resources.qrc \
    sound_effects.qrc

DISTFILES += \
    fonts/Retro Gaming.ttf
