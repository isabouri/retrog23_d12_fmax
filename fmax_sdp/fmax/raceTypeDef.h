/**
 * @file raceTypeDef.h
 * @brief Defines structs used for rendering the race
 */

#ifndef RACETYPEDEF_H
#define RACETYPEDEF_H

#include <QColor>
#include <QPoint>

#include "ship.h"

/**
 * @brief Struct used for displaying ships
 */
struct ShipDisplay
{
    Ship *ship = nullptr; /**< Pointer to the corresponding ship */
    int x; /**< x coordinate of the ship on screen */
    int y; /**< y coordinate ot the ship on screen */
    float relativeDistance; /**< Distance of the ship relative to the camera */
    float twist; /**< Twist amount of the ship in radians */
};

/**
 * @brief Struct used to store temporary information about how to draw all the lines on the screen
 */
struct RenderLine
{
    QColor baseColor; /**< Base color of the road */
    QColor boosterColor; /**< Base color of the booster */
    float shadow; /**< Brightness multiplicator */
    std::pair<QPoint, QPoint> roadPoints; /**< Location of the left and right edges of the lines */
    bool barrier; /**< Presence of a barrier */
    std::pair<QPoint, QPoint> barrierPoints; /**< Location of the left and right edges of the barriers */
    bool heal; /**< This zone being a healing zone */
    bool boosters; /**< This segment containing a booster */
    std::pair<QPoint, QPoint> boosterPoints; /**< Location of the left and right edges of the booster */
    std::list<ShipDisplay *> ships; /**< List of the ships on this line */
};

/**
 * @brief Struct used to save segments of the track
 */
struct Segment
{
    int itsLength; /**< Length of the segments */
    float itsTurnIntensity; /**< Turn intensity of the segment (-1 to 1) */
    float itsTwist; /**< Twist amount (-1 to 1) */
    bool hasBorders; /**< True if this segments had borders */
    bool hasHeal; /**< True if this segments is a healing zone */
    pair<bool, float> itsBooster; /**< First: True if the segment has a booster. Second: Left-right position of the booster*/
};

enum RaceType
{
    NONE, /**< No race selected */
    GP,         /**< Grand Prix race */
    PRACTICE,       /**< Practice race */
};
#endif // RACETYPEDEF_H
