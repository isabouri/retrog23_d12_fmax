/**
 * @file ship.h
 * @brief Defines the Ship class
 */

#ifndef SHIP_H
#define SHIP_H

#include <QImage>
#include <QTimer>
#include <QObject>

#include <vector>
#include <cmath>
#include <iostream>
#include <map>

using namespace std;

#include "shipTypeDef.h"
#include "raceFunctions.h"

/**
 * @brief The Ship class represents a ship in a race
 * @details It inherits from QObject to emit and receive signals
 */
class Ship : public QObject
{
        Q_OBJECT
    private:
        double itsTrueDistance; /**< The distance traveled by the ship since the beginning of the track */
        float itsPosition; /**< The left-right position of the ship on the track */
        float itsDirection; /**< The direction of the ship relative to the direction of the road */
        float itsPower; /**< The power of the ship */
        int itsFallState; /**< 0 if the ship is on the road, negative and descending if is falling */
        QTimer *itsRespawnTimer; /**< Timer before respawning */

        float itsMaxForwardSpeed; /**< Theoritical maximum forward speed of the ship (constant) */
        float itsPossibleForwardSpeed; /**< Maximum forward speed at the moment (affected by turning) */
        float itsForwardSpeed;/**< Current forward speed of the ship */

        float itsMaxSideSpeed;/**< Theoritical maximum side to side speed of the ship (constant) */
        float itsPossibleSideSpeed;/**< Maximum side to side speed of the ship at the moment (affected by forward speed) */
        float itsSideSpeed;/**< Cureent side to side speed of the ship */
        bool outOfCircuit; /**< True if the ship is not on the track */
        bool isRespawnable; /**< True if the ship should respawn upon losing all power */

        bool isOut; /**< true if the ship is running normally */
        bool isHealSignalEmited; /**< True if the heal sound has already been played for the current heal zone */

        int itsRacePostion; /**< Current postion of the ship in the race */
        int itsScore; /**< The score of the ship based on is position at the end of the course. */

        vector<QImage *> itsSprites; /**< Sprites of the ship (360 view) */
        vector<QImage *> itsSpritesExplosion; /**< Explosion sprites */

    public slots:
        /**
         * @brief Makes the ship respawn
         */
        void respawn();

    public:
        /**
         * @brief Ship constructor
         * @param name: Name of the ship used for loading sprites
         * @param startPosition: Position at the beginning of the race
         * @param parent: Pointer to parent QObject
         */
        Ship(QString name, int startPosition, QObject *parent = nullptr);

        ~Ship();
        
        /**
        * @brief Returns the sprite of the ship at a given angle
        * @param angle: Relative angle in radians (0 is the view from behind, grows counter-clockwise)
        * @return Pointer to the sprite
        */
        QImage *getSprite(float angle);

        /**
         * @brief Updates the ship's position from the control input
         * @param userInput: map of the control inputs
         * @param border: presence of a border at this spot in the track
         * @param heal: Presence of a healing zone at this spot in the track
         * @param booster: Presence and location of a booster at this spot in the track
         * @param turn: Turn amount at this location
         * @param gameOver: true if the game is over
         * @param playSound: true if sound should play
         */
        void move(map<Direction, bool> userInput, bool border, bool heal, pair<bool, double> booster, float turn, bool gameOver, bool playSound=true);

        /**
         * @brief Moves the ship automatically
         * @param border: presence of a border at this spot in the track
         * @param heal: Presence of a healing zone at this spot in the track
         * @param booster: Presence and location of a booster at this spot in the track
         * @param turn: Turn amount at this location
         * @param gameOver: true if the game is over
         */
        void moveAI(VisualRoad atShip, bool gameOver);

        /**
         * @brief Bumps the ship when a collision occurs
         * @param angle: angle the impact comes from relative to the ship (in radians, 0 is in front, grows counter-clockwise)
         * @param strength: strenght of the impact
         */
        void bump(float angle, float strength);

        /**
         * @brief Updates the power of the ship and ensures it doesn't go under 0 or over 1
         * @param power: Difference to apply
         */
        void updatePower(float power);

        int getDistance() const;
        float getPosition() const;
        float getDirection() const;
        float getForwardSpeed() const;
        float getItsPower() const;
        int getFallState() const;
        double getTrueDistance() const;
        int getRacePostion() const;
        void setRacePostion(int racePostion);
        void setIsRespawnable(bool newIsRespawnable);
        bool getIsRespawnable() const;
        int getScore() const;
        void setScore(int newScore);

    signals:
        /**
         * @brief Triggered when the ship collides
         */
        void collision();

        /**
         * @brief Triggers when the ship falls off the track
         */
        void fall();

        /**
         * @brief Triggered when the ship explodes
         */
        void explosion();

        /**
         * @brief Triggered when the ships gets a booster
         */
        void boosterSound();

        /**
         * @brief Triggered when the ship starts healing
         */
        void onHeal();
        
        /**
         * @brief Triggered when the ship stops healing
         */
        void notOnHeal();
};

#endif // SHIP_H
