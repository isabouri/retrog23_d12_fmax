#include "soundmanager.h"

SoundManager::SoundManager(QString newMainIntro, QString newMainLoop, QString newLastIntro, QString newLastLoop, QObject *parent) : QObject(parent)
{
    only1Loop = false;
    //set up collision sound effect
    collisionOutput = new QAudioOutput;
    collisionPlayer = new QMediaPlayer;
    collisionPlayer->setSource(QUrl("qrc:/resources/soundEffect/collision.ogg"));
    collisionPlayer->setAudioOutput(collisionOutput);
    collisionOutput->setVolume(COLLISION_VOLUME);

    //set up falling sound effect
    fallSoundOutput = new QAudioOutput;
    fallSoundPlayer = new QMediaPlayer;
    fallSoundPlayer->setSource(QUrl("qrc:/resources/soundEffect/fall.ogg"));
    fallSoundPlayer->setAudioOutput(fallSoundOutput);
    fallSoundOutput->setVolume(FALL_VOLUME);

    //set up booster sound effect
    boosterOutput = new QAudioOutput;
    boosterPlayer = new QMediaPlayer;
    boosterPlayer->setSource(QUrl("qrc:/resources/soundEffect/booster.ogg"));
    boosterPlayer->setAudioOutput(boosterOutput);
    boosterOutput->setVolume(BOOSTER_VOLUME);

    //set up explosion sound effect
    explosionOutput = new QAudioOutput;
    explosionPlayer = new QMediaPlayer;
    explosionPlayer->setSource(QUrl("qrc:/resources/soundEffect/explosion.ogg"));
    explosionPlayer->setAudioOutput(explosionOutput);
    explosionOutput->setVolume(EXPLOSION_VOLUME);

    //set up heal sound effect
    healOutput = new QAudioOutput;
    healPlayer = new QMediaPlayer;
    healPlayer->setSource(QUrl("qrc:/resources/soundEffect/heal.ogg"));
    healPlayer->setAudioOutput(healOutput);
    healOutput->setVolume(HEAL_VOLUME);

    //set up engine sound effect
    engineOutput = new QAudioOutput;
    enginePlayer = new QMediaPlayer;
    enginePlayer->setSource(QUrl("qrc:/resources/soundEffect/engine.ogg"));

    enginePlayer->setAudioOutput(engineOutput);
    engineOutput->setVolume(BASE_ENGINE_VOLUME);
    enginePlayer->setPlaybackRate(0.4);
    enginePlayer->setLoops(QMediaPlayer::Infinite);

    //set up the voice output
    voiceOutput = new QAudioOutput;
    voicePlayer = new QMediaPlayer;
    voicePlayer->setAudioOutput(voiceOutput);
    voiceOutput->setVolume(VOICE_VOLUME);

    //audio output for music
    musicAudioOutput = new QAudioOutput;
    musicAudioOutput->setVolume(MUSIC_VOLUME);

    //init all media player with each parts of the music
    mainIntroPlayer = new QMediaPlayer;
    mainLoopPlayer = new QMediaPlayer;
    lastIntroPlayer = new QMediaPlayer;
    lastLoopPlayer = new QMediaPlayer;

    mainIntroPlayer->setSource(QUrl(newMainIntro));
    mainLoopPlayer->setSource(QUrl(newMainLoop));
    lastIntroPlayer->setSource(QUrl(newLastIntro));
    lastLoopPlayer->setSource(QUrl(newLastLoop));
}


SoundManager::SoundManager(QString newMainIntro, QString newMainLoop, QObject *parent) : QObject(parent)
{
    only1Loop = true;

    //set up collision sound effect
    collisionOutput = new QAudioOutput;
    collisionPlayer = new QMediaPlayer;
    collisionPlayer->setSource(QUrl("qrc:/resources/soundEffect/collision.ogg"));
    collisionPlayer->setAudioOutput(collisionOutput);
    collisionOutput->setVolume(COLLISION_VOLUME);

    //set up falling sound effect
    fallSoundOutput = new QAudioOutput;
    fallSoundPlayer = new QMediaPlayer;
    fallSoundPlayer->setSource(QUrl("qrc:/resources/soundEffect/fall.ogg"));
    fallSoundPlayer->setAudioOutput(fallSoundOutput);
    fallSoundOutput->setVolume(FALL_VOLUME);

    //set up booster sound effect
    boosterOutput = new QAudioOutput;
    boosterPlayer = new QMediaPlayer;
    boosterPlayer->setSource(QUrl("qrc:/resources/soundEffect/booster.ogg"));
    boosterPlayer->setAudioOutput(boosterOutput);
    boosterOutput->setVolume(BOOSTER_VOLUME);

    //set up explosion sound effect
    explosionOutput = new QAudioOutput;
    explosionPlayer = new QMediaPlayer;
    explosionPlayer->setSource(QUrl("qrc:/resources/soundEffect/explosion.ogg"));
    explosionPlayer->setAudioOutput(explosionOutput);
    explosionOutput->setVolume(EXPLOSION_VOLUME);

    //set up heal sound effect
    healOutput = new QAudioOutput;
    healPlayer = new QMediaPlayer;
    healPlayer->setSource(QUrl("qrc:/resources/soundEffect/heal.ogg"));
    healPlayer->setAudioOutput(healOutput);
    healOutput->setVolume(HEAL_VOLUME);

    //set up engine sound effect
    engineOutput = new QAudioOutput;
    enginePlayer = new QMediaPlayer;
    enginePlayer->setSource(QUrl("qrc:/resources/soundEffect/engine.ogg"));

    enginePlayer->setAudioOutput(engineOutput);
    engineOutput->setVolume(BASE_ENGINE_VOLUME);
    enginePlayer->setPlaybackRate(0.4);
    enginePlayer->setLoops(QMediaPlayer::Infinite);

    //set up the voice output
    voiceOutput = new QAudioOutput;
    voicePlayer = new QMediaPlayer;
    voicePlayer->setAudioOutput(voiceOutput);
    voiceOutput->setVolume(VOICE_VOLUME);

    //audio output for music
    musicAudioOutput = new QAudioOutput;
    musicAudioOutput->setVolume(MUSIC_VOLUME);

    //init all media player with each parts of the music
    mainIntroPlayer = new QMediaPlayer;
    mainLoopPlayer = new QMediaPlayer;
    lastIntroPlayer = nullptr;
    lastLoopPlayer = nullptr;

    mainIntroPlayer->setSource(QUrl(newMainIntro));
    mainLoopPlayer->setSource(QUrl(newMainLoop));
}

SoundManager::~SoundManager()
{
    delete mainIntroPlayer;
    delete mainLoopPlayer;
    if(lastIntroPlayer != nullptr)
    {
        delete lastIntroPlayer;
        delete lastLoopPlayer;
    }
    delete musicAudioOutput;
    delete collisionPlayer;
    delete collisionOutput;
    delete fallSoundPlayer;
    delete fallSoundOutput;
    delete boosterPlayer;
    delete boosterOutput;
    delete explosionPlayer;
    delete explosionOutput;
    delete voicePlayer;
    delete voiceOutput;
    delete healPlayer;
    delete healOutput;
    delete enginePlayer;
    delete engineOutput;
}

void SoundManager::startMusic()
{
    //start to play the intro of the baground sound
    mainIntroPlayer->setAudioOutput(musicAudioOutput);
    mainIntroPlayer->play();
    //prepare to launch the loop after the intro
    connect(mainIntroPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(changeMainSound()));
}

void SoundManager::stopMusic()
{
    //stop the track currently playing
    if (mainIntroPlayer->isPlaying())
    {
        mainIntroPlayer->stop();
    }
    if (mainLoopPlayer->isPlaying())
    {
        mainLoopPlayer->stop();
    }
    //not check the last track if the classe has only one track
    if(!only1Loop)
    {
        if (mainIntroPlayer->isPlaying())
        {
            lastIntroPlayer->stop();
        }
        if (mainLoopPlayer->isPlaying())
        {
            lastLoopPlayer->stop();
        }
    }
    //disconnect the following connexion will let the SoundManager play the intro of the music the next time SoundManager::startMusic will be call
    disconnect(mainIntroPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(changeMainSound()));
}

void SoundManager::changeMainSound()
{
    //stop the intro
    mainIntroPlayer->stop();


    //made infinite Loop
    mainLoopPlayer->setAudioOutput(musicAudioOutput);
    mainLoopPlayer->setLoops(QMediaPlayer::Infinite);

    //start the music
    mainLoopPlayer->play();

}

void SoundManager::changeLastSound()
{
    //stop the intro
    lastIntroPlayer->stop();

    //made infinite Loop
    lastLoopPlayer->setAudioOutput(musicAudioOutput);
    lastLoopPlayer->setLoops(QMediaPlayer::Infinite);

    //start the music
    lastLoopPlayer->play();

}


void SoundManager::mainToLast()
{
    if(!only1Loop)
    {
    //stop the main lopp
    mainLoopPlayer->stop();
    lastIntroPlayer->setAudioOutput(musicAudioOutput);
    //then start the last loop's intro and connect to the changeLastSound() slot
    lastIntroPlayer->play();
    connect(lastIntroPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(changeLastSound()));
    }
}

void SoundManager::waitMainLoopEnd()
{
    if (!only1Loop)
    {
        //wait until this loop ends and start the last part of the background music
        mainLoopPlayer->setLoops(1);
        connect(mainLoopPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(mainToLast()));

    }
}

void SoundManager::playCollisionSound()
{
    collisionPlayer->play();
}


void SoundManager::playFallingSound()
{
    fallSoundPlayer->play();
}

void SoundManager::playBoosterSound()
{
    boosterPlayer->play();
}

void SoundManager::playExplosionSound()
{
    explosionPlayer->play();
}

void SoundManager::playHealSound()
{
    healPlayer->play();
}

void SoundManager::stopHealSound()
{
    healPlayer->stop();
}

void SoundManager::playEngineSound()
{
    enginePlayer->play();
}

void SoundManager::stopEngineSound()
{
    enginePlayer->stop();
}

void SoundManager::updateEngineSound(float speed)
{
    engineOutput->setVolume(BASE_ENGINE_VOLUME+speed*ENGINE_VOLUME_MULTIPLIER);
    enginePlayer->setPlaybackRate(0.4+speed*0.1);
}


void SoundManager::playStart()
{
    voicePlayer->setSource(QUrl("qrc:/resources/soundEffect/321go.ogg"));
    voicePlayer->play();
}


void SoundManager::playFinished()
{
    voicePlayer->setSource(QUrl("qrc:/resources/soundEffect/finished.ogg"));
    voicePlayer->play();
}


