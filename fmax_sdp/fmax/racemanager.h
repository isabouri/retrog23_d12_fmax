/**
 * @file racemanager.h
 * @brief Defines the RaceManager class
 */

#ifndef RACEMANAGER_H
#define RACEMANAGER_H

#include <QWidget>
#include <QPainter>
#include <QTimer>
#include <QResizeEvent>
#include <QTextStream>
#include <QFile>
#include <chrono>

#include "soundmanager.h"
#include "raceRenderer.h"


using namespace std::chrono;

namespace Ui {
class RaceManager;
}

/**
 * @brief The RaceManager class is responsible for managing the race
 * @details It is a QWidget that handles moving the ships, checking for collisions among ships, retrieving user input, updating the HUD, telling the SoundManager when to update, and checking when the race ends.
 */
class RaceManager : public QWidget
{
    Q_OBJECT

    public:
        /**
         * @brief Grand Prix mode RaceManager constructor
         * @param filename: path to the track file
         * @param environment: environment number
         * @param nbLaps: number of laps in the race
         * @param parent: pointer to parent QWidget
         */
        explicit RaceManager(QString filename, int environment, int nbLaps, vector<int> positionStart = {1,2,3,4}, QWidget *parent = nullptr);

        /**
         * @brief Practice mode RaceManager constructor
         * @param filename: path to the track file
         * @param environment: environment number
         * @param nbLaps: number of laps in the race
         * @param nbEnnemies: number of ennemies
         * @param respawnChoice: true if ships should respawn
         * @param parent: pointer to parent QWidget
         */
        explicit RaceManager(QString filename, int environment, int nbLaps, int nbEnnemies, bool respawnChoice, QWidget *parent = nullptr);

        ~RaceManager();

        /**
         * @brief Edits the controls for specifics directions.
         * @param direction: The Direction (enum) for which the controls are edited @see Direction
         * @param number: Which control should be modified (primary or secondary)
         * @param key: The key code to set for the specified controls
         */
        static void editControls(Direction direction, int number, int key);

        static map<Direction, int[2]> controls; /**< Static map containing the keys used for input */

        /**
         * @brief Sets the amount of opponents in the race
         * @param nbEnnemies: amount of opponents
         */
        void setNbEnnemies(int nbEnnemies);

        /**
         * @brief Make a list of the score for the GP race.
         * @return Return the list of the points.
         */
        vector<int> getScoreEndGame();

    public slots:
        /**
         * @brief The slot for the game loop
         * @details This function is called at regular intervals by the QTimer to update the race scene and handle game logic.
         */
        void gameLoop();

        /**
         * @brief Slot for the start of the race
         */
        void startGame();

        /**
         * @brief Slot to stop the timers at the end of the race
         */
        void stopTimer();

    signals:
        /**
         * @brief Signal emitted when the race is finished
         */
        void raceFinished();

    private:
        /**
         * @brief Paints the picture on the screen
         * @param event: unused
         */
        void paintEvent(QPaintEvent *event) override;

        /**
         * @brief Updates the attributes when the screen is resized
         * @param event: event containing the new size
         */
        void resizeEvent(QResizeEvent *event) override;

        /**
         * @brief Loads the track from a file to the atrtibute
         * @param filename: resource locator of the track file
         */
        void loadTrack(QString filename);

        /**
         * @brief Calculates the caractesristics of the road at a given distance
         * @param distance: Distance in distance units
         * @return VisualRoad with the corresponding values
         * @see VisualRoad
         */
        VisualRoad determineRoadAtDist(float distance);

        /**
         * @brief Displays the results of the race
         * @param win: true if the player won the race
         */
        void gameEnd(bool win);

        /**
         * @brief Updates the position of the player in the race
         */
        void changePlayerRacePosition();


        Ui::RaceManager *ui; /**< Pointer to the ui */
        QTimer *timer; /**< Pointer to the main game timer */
        QTimer *startCounter; /**< Pointer to the timer for the start countdown */
        QTimer *stopTrack; /**< Pointer to the timer used to wait after the end of the race before closing it */
        time_point<steady_clock> *raceCounter; /**< Pointer to the time of the start of the race */
        RaceRenderer *itsRenderer; /**< Pointer to the race renderer */
        Ship *itsPlayer; /**< Pointer to the player's ship */
        vector<Ship *> *itsShips; /**< Vector contatining the ships in the race. Includes itsPlayers */
        vector<Segment> itsSegments; /**< Vector containing the segments of the race */
        map<Direction, bool> itsUserInput; /**< Map containing the state of the user's inputs */
        QRect *itsLifeBar; /**< Pointer to the QRect of the life bar */
        QRect *borderLifeBar; /**< Pointer to the border of the life bar */
        int itsTotalTrackLength; /**< Total length of the track */
        int itsNbLaps; /**< Total amount of laps in the race */
        bool itsGameOver; /**< True if the game is over */
        bool itsLastLap; /**< True if the player is on the last lap */
        int itsEnvironment; /**< Environment number, 1 to 6 */
        SoundManager *itsSoundManager; /**< Pointer to the sound manager */
        int itsNbEnnemies; /**< Number of opponents in the race */
        RaceType itsRaceType; /**< Type of race (GP or Practice) */

    protected:
        /**
         * @brief Detects key presses to update the state of the controls
         * @param event: event containing the key that was pressed
         * @see RaceManager::itsUserInput
         */
        void keyPressEvent(QKeyEvent *event) override;

        /**
         * @brief Detects key releases to update the state of the controls
         * @param event: event containing the key that was released
         * @see RaceManager::itsUserInput
         */
        void keyReleaseEvent(QKeyEvent *event) override;
};

#endif // RACEMANAGER_H
