#include "raceFunctions.h"

QColor roadColor(int environment, float distance, float totalTrackLength, float relativeDistance, bool heal)
{
    Q_UNUSED(relativeDistance);
    float distanceFromStart = fmod(distance+totalTrackLength, totalTrackLength);

    // Heal
    if (heal)
    {
        return QColor(230*((sin((relativeDistance+0.1*distance)*0.1)+1)/2),230,230*((cos((relativeDistance+0.3*distance)*0.35)+1)/2));
    }

    // Finish line
    if (distanceFromStart < 14)
    {
        if (fmod(distance, 4) > 2)
            return QColor(255, 255, 255);
        else
            return QColor(20, 20, 20);
    }

    // Normal road
    switch (environment)
    {
        case 1:
            if (fmod(distance, 48)<24)
                return QColor(130, 130, 130);
            else
                return QColor(104, 104, 104);
        break;
        case 2:
            switch( (((int)distance)>>5) % 3)
            {
                case 0:
                    return QColor(24, 61, 110); break;
                case 1:
                    return QColor(13, 29, 84); break;
                case 2:
                default:
                    return QColor(8, 8, 48);
            }
        break;
        case 3:
            if (fmod(distance, 64)<16)
                return QColor(200, 200, 200);
            else
                return QColor(220, 220, 220);
        break;
        case 4:
            if (fmod(distance, 64)<10 or fmod(distance, 256)<40)
                return QColor(130, 180, 150);
            else
                return QColor(250, 180, 130);
        break;
        case 5: // rainbow
            if (distance < 0)
                distance += totalTrackLength;
            { // Artificial scope to avoid warnings
                float hue = fmod(distance/400, 1);
                float val;
                if (fmod(distance, 110)<5)
                    val = 0.7;
                else
                    val = 1;
                float sat = (2.8 - 2*pow(sin((relativeDistance-0.8*distance)*0.01), 80))/3;
                return QColor::fromHsvF(hue, sat, val);
            }
        break;
        case 6: // spider
            if (fmod(distance, 48)<10)
                return QColor(255, 0, 0);
            else
            {
                int red = 20;
                red += 100*(pow(sin((relativeDistance-0.8*distance)*0.01), 300));
                red += 100*(pow(sin((relativeDistance-0.7*distance)*0.009), 300));
                return QColor(red, 20, 20);
            }
        break;
        default:
            return QColor(255, 0, 255);
    }
}

QColor boosterColor(float distanceOnTrack, float relativeDistance)
{
    if (fmod(relativeDistance+0.7*distanceOnTrack, 16)<5)
        return QColor(50, 220, 255);
    else
        return QColor(255, 255, 0);
}

void computePoint(int x, short int y, QPoint *currentPoint, float leftLimit, int roadWidth, float roadTwistRadians, int xCenter, int screenHeight, int renderHeight)
{
    // compute the twist of the road
    float xFraction = ((x-leftLimit)/roadWidth)*2 -1;

    float offsetFractionX = cos( roadTwistRadians) * xFraction;
    float offsetFractionY = sin(-roadTwistRadians) * xFraction;

    int offsetPixelX = offsetFractionX * roadWidth/2;
    int offsetPixelY = offsetFractionY * roadWidth/2;

    int xTarget = xCenter+offsetPixelX;
    int yTarget = y+offsetPixelY + screenHeight-renderHeight;

    //set the coordinate of the point
    currentPoint->setX(xTarget);
    currentPoint->setY(yTarget);
}
