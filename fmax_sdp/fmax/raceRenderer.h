/**
 * @file raceRenderer.h
 * @brief Defines the RaceRenderer class
 */

#ifndef RACERENDERER_H
#define RACERENDERER_H

#include <QPainter>

#include "raceTypeDef.h"

/**
 * @brief The RaceRenderer class is responsible for rendering the race
 */
class RaceRenderer
{
    private:
        QSize itsScreenSize; /**< Current size of the screen */
        float itsMiddlePoint[2]; /**< Target point for the end of the road before transforms */
        float itsRoadWidth; /**< Width of the road at the bottom of the screen */
        float itsRoadEndWidth; /**< Width of the road at the end */
        float itsTurnIntensity; /**< Intensity of the turn */
        float itsTwist; /**< Intensity of the twist */
        float itsCameraPosition; /**< Left-right position of the camera */
        int itsRenderHeight; /**< Height of the camera */
        float itsAverageTurnGap; /**< Average value to offset by for turns */
        vector<RenderLine> itsRenderLines; /**< All the RenderLines to display */
        list<Ship *> itsUndrawnShips; /**< List of the ships that haven't been displayed yet on this frame */
        Ship *itsPlayer; /**< Pointer to the player's ship */
        int itsTotalTrackLength; /**< Total length of the track */
        int itsEnvironment; /**< Environment of the track, number from 1 to 6 */

        /**
         * @brief Computes the position of the points to draw in part 2
         * @param distance: Current distance since the beginning of the race
         * @param barriers: Presence of barriers for the next 261 distance units
         * @param heals: Presence of a healing zone for the next 261 distance units
         * @param boosters: Presence of location of boosters for the next 261 distance units
         */
        void drawPart1(float distance, bool barriers[261], bool heals[261], pair<bool, float> boosters[261]);

        /**
         * @brief Draws the second part of the race scene using a QPainter object based on all points calculated
         * @param painter The QPainter object used for drawing
         */
        void drawPart2(QPainter *painter);

    public:
        /**
         * @brief RaceRenderer constructor
         * @param totalTrackLength: total length of the track
         * @param environment
         */
        RaceRenderer(int totalTrackLength, int environment);

        /**
         * @brief Updates the attributes if the size of the screen changes
         * @param screenSize: New size of the screen
         */
        void setScreenSize(const QSize &screenSize);

        /**
         * @brief handles setting the variables before drawing starting the draw process
         * @param painter: Pointer to the QPainter
         * @param turn: Turn intensity
         * @param twist: Twist intensity
         * @param barriers: Presence of barriers for the next 261 distance units
         * @param heals: Presence of a healing zone for the next 261 distance units
         * @param boosters: Presence and location of boosters for the next 261 distance units
         * @param shipsOnTrack: Vector containing all the ships on the track
         */
        void drawManager(QPainter *painter, float turn, float twist, bool barriers[261], bool heals[261], pair<bool, float> boosters[261], vector<Ship *> *shipsOnTrack);

        /**
         * @brief Used to define which ship is the player's
         * @param player: Pointer to the player
         */
        void setPlayer(Ship *player);

        /**
         * @brief Updates the left-right position of the camera
         */
        void updateCameraPosition();
};

#endif // RACERENDERER_H
