/**
 * @file widget.h
 * @brief Defines the main Widget
 */

#ifndef WIDGET_H
#define WIDGET_H

#include <QDialog>
#include <QValidator>
#include <QWidget>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

#include "racemanager.h"
#include "gpmanager.h"
#include "typeDef.h"

/**
 * @brief The Widget class handles all the menu navigation and launches the race manager
 */
class Widget : public QWidget
{
    Q_OBJECT

    public:
        /**
         * @brief Widget constructor
         * @param parent: The parent widget
         */
        Widget(QWidget *parent = nullptr);

        ~Widget();
        
    private slots:
        /**
         * @brief Handles the race finished event
         */
        void handleRaceFinished();

        void on_quitButton_clicked();
        void on_playButton_clicked();
        void on_returnButton_clicked();
        void on_settingsButton_clicked();
        void on_returnButton_2_clicked();
        void on_grandPrixButton_clicked();
        void on_practiceButton_clicked();
        void on_pushButton_Iron_Practice_clicked();
        void on_pushButton_Y_clicked();
        void on_pushButton_Bean_clicked();
        void on_pushButton_D2_clicked();
        void on_pushButton_Swing_clicked();
        void on_pushButton_8__clicked();
        void on_pushButton_Maxime_clicked();
        void on_pushButton_Tron_clicked();
        void on_pushButton_TRex_clicked();
        void on_pushButton_81__clicked();
        void on_pushButton_Zen_clicked();
        void on_pushButton_Titanium_Practice_clicked();
        void on_pushButton_Ruby_Practice_clicked();
        void on_pushButton_Lucky_You_clicked();
        void on_pushButton_VegaMissyl_clicked();
        void on_pushButton_PridesRoad_clicked();
        void on_pushButton_SpaceTraveller_clicked();
        void on_pushButton_SpiderDen_clicked();
        void on_returnToSelectCup_Ruby_clicked();
        void on_returnToSelectCup_Titanium_clicked();
        void on_returnToSelectCup_Iron_clicked();
        void on_returnToMenuMode_clicked();
        void on_lineEditForward_editingFinished();
        void on_lineEditBrake_editingFinished();
        void on_lineEditLeft_editingFinished();
        void on_lineEditRight_editingFinished();
        void on_tutoButton_clicked();
        void on_returnToChoseMap_clicked();
        void on_startRaceAfterParam_clicked();
        void on_nbToursPractice_editingFinished();
        void on_nbVaisseauxPractice_editingFinished();
        void on_stackedWidget_currentChanged(int arg1);

    private:
        /**
        * @brief Repaints the window
        * @param event: unused
        */
        void paintEvent(QPaintEvent *event);

        /**
        * @brief Updates attributes when the widget is resized
        * @param event: event containing the new size
        */
        void resizeEvent(QResizeEvent *event);

        /**
        * @brief Calls a new race manager
        */
        void startRace();

        /**
        * @brief Starts the next race of the GP
        */
        void startNextGPRace();

        RaceManager *itsRaceManager; /**< Pointer to the race manager */
        SoundManager *itsSoundManager; /**< Pointer to the sound manager */
        GPManager itsGPManager; /**< Grand Prix score manager */

        void changeSettings();

        QString itsTrackName; /**< Name of the track */
        int itsEnvironment; /**< Environment number (1 to 6) */
        int nbEnnemies; /**< Number of opponents in the race */
        int itsNbLaps; /**< Number of laps in the race */
        QDialog popupDialog; /**< Popup for tutorial */
        Ui::Widget *ui; /**< Pointer to the widget's ui */
        QPixmap itsMenuBackground; /**< Background image of the menu */
        QString itsBackgroundName; /**< QRessouce path of the background image */
        QLabel *itsTutoLabel; /**< Label for the tutorial */
        QRegularExpressionValidator *itsKeyValidator; /**< Validator for the key setting input */
        QRegularExpressionValidator *itsNbEnnemyValidator; /**< Validator for the number of ennemies */
        QRegularExpressionValidator *itsNbLapsValidator; /**< Validator for the number of laps */
        RaceType itsTypeOfRace; /**< The type of the race */
        list< pair<QString, pair<int, int>>> itsGPListRace; /**< A list of race whitch is a cup*/
        list< pair<QString, pair<int, int>>>::iterator itGPRace; /**< The iterator of the cup to parcour each race */

        vector<int> itsLastGPScore; /**< The list of the score of the last race each player  */
        bool GPCanGoNextRace; /**< True if grand prix tracks remain */
        
    protected:
        void keyPressEvent(QKeyEvent *event);
};

#endif // WIDGET_H
