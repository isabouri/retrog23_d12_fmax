/**
 * @file raceFunctions.h
 * @brief Defines functions used for rendering the race
 */

#ifndef RACEFUNCTIONS_H
#define RACEFUNCTIONS_H

#include <QColor>
#include <QPoint>

#include <cmath>

/**
 * @brief Returns the color of the road at a given point
 * @param environment: Number of the environment 1 through 6
 * @param distance: Distance since the beginning of the race
 * @param totalTrackDistance: Total length of a lap
 * @param relativeDistance: Distace relative to the camera (for effects)
 * @param heal: true if that segment is a heal
 * @return Corresponding color
 */
QColor roadColor(int environment, float distance, float totalTrackDistance, float relativeDistance, bool heal);

/**
 * @brief Returns the color of the booster at a given point
 * @param distanceOnTrack: Distance since the beginning of the race
 * @param relativeDistance: distance from the camera (for effects)
 * @return Corresponding color
 */
QColor boosterColor(float distanceOnTrack, float relativeDistance);

/**
 * @brief Computes the location of a point (polygon corner for the road)
 * @param x: left-right location of the point before transforms
 * @param y: line on the render
 * @param currentPoint: adress of the QPoint to save the data to
 * @param leftLimit: Left limit of the road at this point
 * @param roadWidth: width of the road at this point
 * @param roadTwistRadians: twist amount
 * @param xCenter: left-right location of the center of the line
 * @param screenHeight: height of the screen
 * @param renderHeight: height of the rendered part
 */
void computePoint(int x, short int y, QPoint *currentPoint, float leftLimit, int roadWidth, float roadTwistRadians, int xCenter, int screenHeight, int renderHeight);

/**
 * @brief Caps a value between a minimum and a maximum
 * @param min: minimum value (included)
 * @param input: input value
 * @param max: maximum value (included)
 * @return capped value
 */
template <class T>
T cap(T min, T input, T max)
{
    return (input<min)?min:((input>max)?max:input);
}

#endif // RACEFUNCTIONS_H
