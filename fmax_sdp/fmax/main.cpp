#include "widget.h"

#include <QApplication>
#include <QFontDatabase>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //Adding fonts to application's font database to use it in UI
    QFontDatabase::addApplicationFont(":/resources/resources/font/Retro Gaming.ttf");
    QFontDatabase::addApplicationFont(":/resources/resources/font/joystix monospace.otf");
    Widget w;
    w.show();
    return a.exec();
}
