#ifndef TYPEDEF_H
#define TYPEDEF_H

#include <QString>

struct ScoreLine
{
    QString name; /**< Natural language name of the ship */
    int lastScore; /**< Score in the last race */
    int totalScore; /**< Total score since the beginning of the grand prix */
    int shipNumber; /**< Number of the ship */
};

#endif // TYPEDEF_H
