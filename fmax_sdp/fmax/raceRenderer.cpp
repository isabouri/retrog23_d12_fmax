#include "raceRenderer.h"

RaceRenderer::RaceRenderer(int totalTrackLength, int environment)
    : itsTotalTrackLength(totalTrackLength),
      itsEnvironment(environment)
{
    itsMiddlePoint[0] = 0.5;
    itsMiddlePoint[1] = 0.374;
    itsRoadWidth = 2.58;
    itsRoadEndWidth = 0.01;
    itsCameraPosition = 0.5;
    itsTwist = 0;
    itsTurnIntensity = 9.75002;
}

void RaceRenderer::drawPart1(float distance, bool barriers[261], bool heals[261], pair<bool, float> boosters[261])
{
    itsRenderHeight = ceil(itsScreenSize.height()*(1-itsMiddlePoint[1]));

    //the scale value (min : 0, max : 1) of the left edge of the road, init with the value at the bottom of the screen
    float leftEdge = -(itsRoadWidth-1)*(itsCameraPosition*1.63 - 0.315);
    //the scale value (min : 0, max : 1) of the right edge of the road, init with the value at the bottom of the screen
    float rightEdge = -(itsRoadWidth-1)-leftEdge;

    // How many pixels to reduce the width of the road by on every line
    float leftGap =  (itsScreenSize.width()*(0.5-leftEdge -(itsRoadEndWidth/2) ))  /  itsRenderHeight;
    float rightGap = (itsScreenSize.width()*(0.5-rightEdge -(itsRoadEndWidth/2)))  /  itsRenderHeight;

    //compute the gap between each lines to draw a turn
    itsAverageTurnGap = (0.5-itsMiddlePoint[0]) * itsScreenSize.width()/itsRenderHeight;
    float turnGap = 0;
    //The pixel abscissa of the road left edge, init with the value at rhe bottom of the screen
    float leftLimit = itsScreenSize.width()*leftEdge;
    //The pixel abscissa of the road right edge, init with the value at rhe bottom of the screen
    float rightLimit = itsScreenSize.width()*(1-rightEdge);

    // set up each lines of the road
    for (short int y = itsRenderHeight-1; y >= 0; --y)
    {
        RenderLine thisLine; // used to set up each lines

        int roadWidth = rightLimit - leftLimit;
        float roadDistancePercent = (y+1-itsRenderHeight)/(1.0-itsRenderHeight);
        // Arbitraty distance -> arbitratry constants in the formula
        // Approximate outputs: 0->0, 1->261
        float roadDistance = (-1.9)/tan(roadDistancePercent*0.172 - 0.179) - 10.5;

        float roadTwistRadians = itsTwist * roadDistance * (3.1415926/180);

        float visualDistance = sqrt(roadDistance*roadDistance + 144);
        //compute the distance shadow
        float shadowMultiplier = 1-pow(visualDistance/260, 1);

        thisLine.baseColor = roadColor(itsEnvironment, distance + roadDistance, itsTotalTrackLength, roadDistance, heals[(int)roadDistance]);
        thisLine.shadow = shadowMultiplier;
        int xCenter = leftLimit + (roadWidth>>1);

        // add a border
        thisLine.barrier = barriers[(int)roadDistance];
        // add heal
        thisLine.heal = heals[(int)roadDistance];
        thisLine.boosters = boosters[(int)roadDistance].first;


        // add border coordinate if there is border on this part of the road
        if (not thisLine.barrier)
            thisLine.barrierPoints = {QPoint(0, 0), QPoint(0, 0)} ;

        computePoint(leftLimit+1, y, &thisLine.roadPoints.first, leftLimit, roadWidth, roadTwistRadians, xCenter, itsScreenSize.height(), itsRenderHeight);
        computePoint(rightLimit, y, &thisLine.roadPoints.second, leftLimit, roadWidth, roadTwistRadians, xCenter, itsScreenSize.height(), itsRenderHeight);

        if (thisLine.barrier)
        {
            computePoint(leftLimit+1 - roadWidth/8, y, &thisLine.barrierPoints.first, leftLimit, roadWidth, roadTwistRadians, xCenter, itsScreenSize.height(), itsRenderHeight);
            computePoint(rightLimit + roadWidth/8, y, &thisLine.barrierPoints.second, leftLimit, roadWidth, roadTwistRadians, xCenter, itsScreenSize.height(), itsRenderHeight);
        }

        if (thisLine.boosters)
        {
            thisLine.boosterColor = boosterColor(distance, roadDistance);
            int boosterCenter = leftLimit+1 + roadWidth*boosters[(int)roadDistance].second;
            int leftBoosterLimit = boosterCenter - roadWidth/8;
            int rightBoosterLimit = boosterCenter + roadWidth/8;

            computePoint(leftBoosterLimit, y, &thisLine.boosterPoints.first, leftLimit, roadWidth, roadTwistRadians, xCenter, itsScreenSize.height(), itsRenderHeight);
            computePoint(rightBoosterLimit, y, &thisLine.boosterPoints.second, leftLimit, roadWidth, roadTwistRadians, xCenter, itsScreenSize.height(), itsRenderHeight);
        }

        // select the ship which must be display at the current line
        list<Ship *> shipsToForgetThisLine;
        for (Ship *ship : itsUndrawnShips)
        {
            float previousRoadDistance = (-1.9)/tan(((y+2-itsRenderHeight)/(1.0-itsRenderHeight))*0.172 - 0.179) - 10.5;
            float shipRelDist = fmod(fmod(ship->getTrueDistance()-distance, itsTotalTrackLength)+itsTotalTrackLength, itsTotalTrackLength);
            if (previousRoadDistance <= shipRelDist and shipRelDist < roadDistance)
            {
                //create a ShipDisplay struct to store information about each ship which must be displayed
                shipsToForgetThisLine.push_back(ship);
                ShipDisplay *shipDisplay = new ShipDisplay;
                shipDisplay->ship = ship;
                shipDisplay->x = thisLine.roadPoints.first.x() + ship->getPosition()*(thisLine.roadPoints.second.x()-thisLine.roadPoints.first.x());
                shipDisplay->y = thisLine.roadPoints.first.y() + ship->getPosition()*(thisLine.roadPoints.second.y()-thisLine.roadPoints.first.y()) + pow((-ship->getFallState()), 1.6);
                shipDisplay->relativeDistance = 5+sqrt(shipRelDist*shipRelDist + pow((3-ship->getFallState()), 2));
                shipDisplay->twist = roadTwistRadians;
                thisLine.ships.push_back(shipDisplay);
            }
        }
        // removes the ship of this line
        for (Ship *ship : shipsToForgetThisLine)
            itsUndrawnShips.remove(ship);

        itsRenderLines.push_back(thisLine);

        //update the turning gap each ordonate to draw a turn
        turnGap = itsAverageTurnGap*exp((itsTurnIntensity * ((itsRenderHeight-y) / (float)itsRenderHeight))); // use an exponential function to simulate a turn
        //update the value of each limit
        leftLimit += leftGap + turnGap;
        rightLimit += - rightGap + turnGap;
    }
}

void RaceRenderer::drawPart2(QPainter *painter)
{
    // set up the value of the pen with the color of the sky
    painter->setBrush(QColor(0, 0, 0));
    painter->setPen(Qt::NoPen);
    //fill all the screen with the sky color
    painter->drawRect(0,0,itsScreenSize.width(),itsScreenSize.height());

    //all points needed to draw the polygons
    QPoint  newRoadLeft,    newRoadRight,    oldRoadLeft,    oldRoadRight,
            newBarrierLeft, newBarrierRight, oldBarrierLeft, oldBarrierRight,
            newBoosterLeft, newBoosterRight, oldBoosterLeft, oldBoosterRight;
    bool barrierBefore = itsRenderLines[itsRenderHeight-1].barrier;
    bool boostBefore = itsRenderLines[itsRenderHeight-1].boosters;
    QColor roadCol;

    // Init the loop
    // Retrieve all values of the first line at the top of the road.
    oldRoadLeft = itsRenderLines[itsRenderHeight-1].roadPoints.first;
    oldRoadRight = itsRenderLines[itsRenderHeight-1].roadPoints.second;
    oldBarrierLeft = itsRenderLines[itsRenderHeight-1].barrierPoints.first;
    oldBarrierRight = itsRenderLines[itsRenderHeight-1].barrierPoints.second;
    oldBoosterLeft = itsRenderLines[itsRenderHeight-1].boosterPoints.first;
    oldBoosterRight = itsRenderLines[itsRenderHeight-1].boosterPoints.second;

    // this loop iterate trough all the computed lines and draw poligon between each line and the previous one
    for (short int y = itsRenderHeight-1; y > -1; --y)
    {
        // Retrieve all values of the current line at the top of the road.
        if (itsEnvironment == 5)
            roadCol = itsRenderLines[y].baseColor.lighter((itsRenderLines[y].shadow/2 + 0.5) * 100);
        else
            roadCol = itsRenderLines[y].baseColor.lighter(itsRenderLines[y].shadow * 100);
        QColor barrierCol = QColor(255, 0, 0).lighter(itsRenderLines[y].shadow * 100);
        newRoadLeft = itsRenderLines[y].roadPoints.first;
        newRoadRight = itsRenderLines[y].roadPoints.second;
        newBarrierLeft = itsRenderLines[y].barrierPoints.first;
        newBarrierRight = itsRenderLines[y].barrierPoints.second;
        newBoosterLeft = itsRenderLines[y].boosterPoints.first;
        newBoosterRight = itsRenderLines[y].boosterPoints.second;

        // draw the polygon between the current lien and the previous ones
        painter->setBrush(roadCol);
        painter->drawPolygon(QPolygon((QVector<QPoint>{newRoadLeft, newRoadRight, oldRoadRight, oldRoadLeft})));
        if (barrierBefore and itsRenderLines[y].barrier)
        {
            painter->setBrush(barrierCol);
            painter->drawPolygon(QPolygon((QVector<QPoint>{newBarrierLeft, newRoadLeft, oldRoadLeft, oldBarrierLeft})));
            painter->drawPolygon(QPolygon((QVector<QPoint>{newRoadRight, newBarrierRight, oldBarrierRight, oldRoadRight})));
        }

        if (boostBefore and itsRenderLines[y].boosters)
        {
            painter->setBrush(itsRenderLines[y].boosterColor);
            painter->drawPolygon(QPolygon((QVector<QPoint>{newBoosterLeft, newBoosterRight, oldBoosterRight, oldBoosterLeft})));
        }


        // display the ships on this line
        for (ShipDisplay *shipDisplay : itsRenderLines[y].ships)
        {
            //compute the size of the ship scaled with the distance
            QSize size(48*(itsRenderHeight/8)/shipDisplay->relativeDistance, 32*(itsRenderHeight/8)/shipDisplay->relativeDistance);

            // compute values used later in the matrix calculation
            float roadWidth = sqrt(pow((newRoadLeft.x()-newRoadRight.x()), 2) + pow((newRoadLeft.y()-newRoadRight.y()), 2));
            //angle in radius
            float angleToCamera = atan((shipDisplay->ship->getPosition() - itsCameraPosition)*(roadWidth) / shipDisplay->relativeDistance)/13;
            float angleFromRoad = itsAverageTurnGap*exp((itsTurnIntensity * ((y) / (float)itsRenderHeight)))*shipDisplay->relativeDistance/(-1700);

            // Matrix transform for rotation
            float m11, m12, m21, m22;
            m22 = m11 = cos(shipDisplay->twist);
            m21 = sin(shipDisplay->twist);
            m12 = -m21;
            float totalAngle = angleToCamera + angleFromRoad + shipDisplay->ship->getDirection();
            QImage sprite = shipDisplay->ship->getSprite(totalAngle)->scaled(size);
            int tx = sprite.width();
            int ty = sprite.height();

            QTransform rotation(m11, m12, 0,
                                m21, m22, 0,
                                0, 0, 1);

            sprite = sprite.transformed(rotation);

            int xmin =  min
                        (
                            min
                            (
                                shipDisplay->x + sin(-shipDisplay->twist)*ty + cos(-shipDisplay->twist)*(tx/2),
                                shipDisplay->x + cos(-shipDisplay->twist)*(tx/2)
                            ),
                            min
                            (
                                shipDisplay->x - cos(-shipDisplay->twist)*(tx/2),
                                shipDisplay->x + sin(-shipDisplay->twist)*ty - cos(-shipDisplay->twist)*(tx/2)
                            )
                        );

            int ymin =  min
                        (
                            min
                            (
                                shipDisplay->y + sin(-shipDisplay->twist)*((tx)/(2)) - cos(-shipDisplay->twist)*ty,
                                shipDisplay->y + sin(-shipDisplay->twist)*((tx)/(2))
                            ),
                            min
                            (
                                shipDisplay->y - sin(-shipDisplay->twist)*((tx)/(2)),
                                shipDisplay->y - sin(-shipDisplay->twist)*((tx)/(2)) - cos(-shipDisplay->twist)*ty
                            )
                        );
            painter->drawImage(xmin, ymin, sprite);
            delete shipDisplay;
        }

        // update the values of the old line with the current line for the next iteration
        oldRoadLeft = newRoadLeft;
        oldRoadRight = newRoadRight;
        oldBarrierLeft = newBarrierLeft;
        oldBarrierRight = newBarrierRight;
        oldBoosterLeft = newBoosterLeft;
        oldBoosterRight = newBoosterRight;
        barrierBefore = itsRenderLines[y].barrier;
        boostBefore = itsRenderLines[y].boosters;

    }
}

void RaceRenderer::drawManager(QPainter *painter, float turn, float twist, bool barriers[261], bool heals[261], pair<bool, float> boosters[261], vector<Ship *> *shipsOnTrack)
{
    // set up the shipsOnTracks vector for draw funtions
    itsUndrawnShips.clear();
    for (Ship *ship : *shipsOnTrack)
        itsUndrawnShips.push_back(ship);

    // compute the player camera values with its coordtinates
    itsMiddlePoint[0] = 0.5 - turn*0.00035;
    itsTwist = twist;

    // call draw functions
    drawPart1(itsPlayer->getTrueDistance() - 4 - 0.8*itsPlayer->getForwardSpeed(), barriers, heals, boosters);
    drawPart2(painter);

    // empty the vector containing each lines' informations after use
    itsRenderLines.clear();
}

void RaceRenderer::setPlayer(Ship *player)
{
    this->itsPlayer = player;
    itsCameraPosition = itsPlayer->getPosition();
}

void RaceRenderer::updateCameraPosition()
{
    itsCameraPosition = cap(-0.1f, itsPlayer->getPosition(), 1.1f)*0.2 + itsCameraPosition*0.8; // Smoothing
}

void RaceRenderer::setScreenSize(const QSize &ScreenSize)
{
    itsScreenSize = ScreenSize;
}
