#include "gpmanager.h"

GPManager::GPManager()
{
    reset();
}

void GPManager::updateScores()
{
    for (int i=0; i<4; ++i)
    {
        itsTotalScores[i] += itsLastScores[i];
        itsLastScores[i] = 0;
    }
}

void GPManager::reset()
{
    itsTotalScores.clear();
    itsLastScores.clear();
    for (int i=0; i<4; ++i)
    {
        itsTotalScores.push_back(0);
        itsLastScores.push_back(0);
    }
}

int GPManager::getTotalScore(int index) const
{
    return itsTotalScores[index];
}

int GPManager::getLastScore(int index) const
{
    return itsLastScores[index];
}

void GPManager::setLastScore(int index, int lastScore)
{
    itsLastScores[index] = lastScore;
}
